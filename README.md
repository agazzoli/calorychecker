# README #

* This repository hosts an application called CaloryChecker which is intended to be used as a comprehensive example and a base code for future developments.

### How do I get set up? ###

* Dependencies
    * MongoDB
    * NodeJS
    * NPM
    * Protractor
    * tsd
    * grunt
* Configuration
    * Server
        * npm install
    * Client
        * npm install
* Database and server configuration
    * In server/app/config.ts file.
* How to run tests
    * Server tests
        * grunt initial_data_test
        * grunt test
    * Client tests
        * grunt test
    * End to end tests
        * grunt initial_data_test
        * grunt start_test
        * grunt e2e_test
* Deployment instructions
    * Client
        * grunt build
* How to run
    * Server
        * grunt initial_data_pro
        * grunt start_pro
    * Client
        * Navigate to https://localhost:8080


### Who do I talk to? ###

* Andres Gazzoli (andresgazzoli@gmail.com)