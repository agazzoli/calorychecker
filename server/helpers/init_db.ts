/// <reference path='../typings/node/node.d.ts' />

let mongoose = require('mongoose');
let User = require('../app/model/user');

module.exports = function(database, done) {

    mongoose.connect(database, function(){
        mongoose.connection.db.dropDatabase(function() {
            let user = new User({
                username: 'admin',
                password: "$2a$10$1b3dqu8uv9dbJP9RQKWpzuwqZmhCriIJMC75jX0NIst5qNW66qpVi",
                admin: true,
                calories_expected_per_day: 0
            });

            user.save(function() {
                console.log('admin user was added!');
                done();
            });
        });
    });
};