/// <reference path='../typings/node/node.d.ts' />

module.exports = function() {
    return {
        server: {
            'development': {
                'protocol': 'https',
                'host': 'localhost',
                'port': '8080'
            },
            'test': {
                'protocol': 'https',
                'host': 'localhost',
                'port': '8080'
            },
            'production': {
                'protocol': 'https',
                'host': 'localhost',
                'port': '8080'
            }
        },
        client: {
            'development': {
                'folder': 'app'
            },
            'test': {
                'folder': 'app'
            },
            'production': {
                'folder': 'dist'
            }
        },
        database: {
            'development': 'mongodb://127.0.0.1:27017/appdb-dev',
            'test': 'mongodb://127.0.0.1:27017/appdb-test',
            'production': 'mongodb://127.0.0.1:27017/appdb'
        },
        jwt_key: {
            'development': 't4f9h61juy',
            'test': 'tgasejud',
            'production': 'd4fah78jyu'
        }
    }
}();

