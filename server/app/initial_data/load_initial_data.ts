/// <reference path='../../typings/node/node.d.ts' />

let config = require('../config');
let init_db = require('../../helpers/init_db');

if (process.argv.length < 3) {
    console.log('<usage> node load_initial_data.js [development | test | production]');
    process.exit();
}

let database = config.database[process.argv[2]];

init_db(database, function() {
    console.log('Done!');
    process.exit();
});

