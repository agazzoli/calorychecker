/// <reference path='../typings/node/node.d.ts' />

//-----------------------------------------------
// Libraries

// Express.
let express = require('express');

// https.
var fs = require('fs');
var https = require('https');

// Utils.
let bodyParser = require('body-parser');
let expressValidator = require('express-validator');
let morgan = require('morgan');
let mongoose = require('mongoose');
let jwt = require('jsonwebtoken');

// Own.
let config = require('./config');
let routes = require('./routes/routes');

//-----------------------------------------------
// Set run mode if it's present in command line.
if (process.argv.length === 3)
    process.env.NODE_ENV = process.argv[2];

//-----------------------------------------------
// Express application.
let app = express();

//-----------------------------------------------
// Custom validators.

app.use(expressValidator({
    customValidators: {
        isMealId: function(value) {
            return mongoose.Types.ObjectId.isValid(value);
        }
    }
}));

//-----------------------------------------------
// Config.

// Connect to database.
mongoose.connect(config.database[app.settings.env]);

app.set('jwt_key', config.jwt_key[app.settings.env]);

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Use to validate parameters.
app.use(expressValidator());

// Log requests to the console.
app.use(morgan('dev'));

// Serve static files.
app.use(express.static(__dirname + '/../../client/' + config.client[app.settings.env].folder));

var https_options = {
    key  : fs.readFileSync(__dirname + '/cert/server.key'),
    cert : fs.readFileSync(__dirname + '/cert/server.crt')
};

//-----------------------------------------------
// Routes.

// Apply routes with the prefix /api.
app.use('/api', routes.user_basic(app));
app.use('/api', routes.authentication(app));
app.use('/api', routes.user());
app.use('/api', routes.meal());

//-----------------------------------------------
// Starting Web Server.
https.createServer(https_options, app).listen(config.server[app.settings.env].port, config.server[app.settings.env].host);
console.log('Web Server listening at ' + config.server[app.settings.env].host + ':' + config.server[app.settings.env].port);
