/// <reference path='../../typings/node/node.d.ts' />
/// <reference path='../../typings/express/express.d.ts' />
/// <reference path='../../typings/jsonwebtoken/jsonwebtoken.d.ts' />
/// <reference path='../../typings/express-validator/express-validator.d.ts' />

let jwt = require('jsonwebtoken');
let bcrypt = require('bcryptjs');
let User = require('./../model/user');

module.exports = function(app) {

    return {
        // Sign up.
        signup: function(req, res) {

            // Check body parameters.
            req.checkBody('username', "Username can not be empty or undefined!").notEmpty();
            req.checkBody('password', "Password can not be empty or undefined!").notEmpty();
            req.checkBody('calories_expected_per_day', "Calories expected per day should be an integer!").isInt();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get username and password from body.
            let username:string = req.body.username;
            let password:string = req.body.password;
            let calories_expected_per_day:number = req.body.calories_expected_per_day;

             // Check if the user name is not already used.
            User.findOne({username: username}, function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Sign up failed. Server error!"});
                    return;
                }

                if (user) {
                    res.status(404).json({success: false, msg: 'Username already used!'});
                    return;
                }
                else {

                    bcrypt.hash(password, 10, function(err, hash) {

                        if (err) {
                            res.status(500).json({success: false, msg: "Sign up failed. Server error!"});
                            return;
                        }

                        let user = new User({
                            username: username,
                            password: hash,
                            admin: false,
                            calories_expected_per_day: calories_expected_per_day
                        });

                        // Save the new user.
                        user.save(function (err) {

                            if (err) {
                                res.status(500).json({success: false, msg: "Sign up failed. Server error!"});
                                return;
                            }

                            res.status(200).json({success: true, msg: 'User successfully signed up!'});
                        });

                    });
                }
            });
        },

        // Log in.
        login: function(req, res) {

            // Check body parameters.
            req.checkBody('username', "Username can not be empty or undefined!").notEmpty();
            req.checkBody('password', "Password can not be empty or undefined!").notEmpty();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get username and password from body.
            let username:string = req.body.username;
            let password:string = req.body.password;

            // Find the user.
            User.findOne({username: username}).select('+password').exec(function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Log in failed. Server error!"});
                    return;
                }

                if (!user) {
                    res.status(404).json({success: false, msg: 'User or password wrong!'});
                    return;
                }
                else {

                    bcrypt.compare(password, user.password, function(err, password_match) {
                        // Check if password matches.
                        if (password_match == false) {
                            res.status(404).json({success: false, msg: 'User or password wrong!'});
                            return;
                        }
                        else {
                            // Create a token.
                            let token:string = jwt.sign(user, app.get('jwt_key'), {
                                expiresIn: "2h"
                            });

                            // Removing password to avoid sending it.
                            let temp:any = user.toObject();
                            delete temp.password;

                            // Return the information including token as JSON.
                            res.status(200).json({
                                success: true,
                                msg: 'User successfully logged in!',
                                token: token,
                                user: temp
                            });
                        }
                    });
                }
            });
        }
    }
};