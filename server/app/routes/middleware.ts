/// <reference path='../../typings/node/node.d.ts' />
/// <reference path='../../typings/jsonwebtoken/jsonwebtoken.d.ts' />

let jwt = require('jsonwebtoken');

module.exports = function(app) {

    return {
        // Route middleware to verify a token.
        authentication: function(req, res, next)
        {
            // Check header or url parameters or post parameters for token.
            let token:string = req.body.token || req.query.token || req.headers['authentication'];
    
            if (token) {
                // Check token.
                jwt.verify(token, app.get('jwt_key'), function (err, decoded) {
                    if (err) {
                        return res.status(403).json({success: false, msg: 'Failed to authenticate token!'});
                    }
                    else {
                        // if everything is good, save to request for use in other routes
                        req.decoded = decoded;
                        next();
                    }
                });
            }
            else {
                return res.status(403).send({
                    success: false,
                    msg: 'No token provided!'
                });
            }
        }
    }
};