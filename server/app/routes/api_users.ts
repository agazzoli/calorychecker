/// <reference path='../../typings/node/node.d.ts' />

let bcrypt = require('bcryptjs');
let User = require('./../model/user');
let Meal = require('./../model/meal');

module.exports = function() {

    return {
        // Get user list. Only available for Admin user.
        get_users: function(req, res) {

            var isAdmin:boolean = req.decoded._doc.admin;

            // Check if user is admin.
            if (!isAdmin) {
                res.status(403).json({success: false, msg: 'User is not administrator!'});
                return;
            }

            User.find({}, 'username admin calories_expected_per_day', function (err, users) {

                if (err) {
                    res.status(500).json({success: false, msg: "Get users failed. Server error!"});
                    return;
                }

                return res.status(200).json({success: true, msg: "Users successfully retrieved!", users: users});
            });
        },

        // Get a user.
        get_user: function(req, res) {

            // Get user from token.
            let username:string = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to return its info.
            let username_to_apply_action:string = req.params.username;

            if (username !== username_to_apply_action) {

                // Check if user is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }
            }
    
            // Find the user.
            User.findOne({username: username_to_apply_action}, function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Get settings failed. Server error!"});
                    return;
                }

                if (!user) {
                    res.status(404).json({success: false, msg: 'User not found!'});
                    return;
                }
                else {
                    res.status(200).json(
                        {
                            success: true,
                            msg: 'User settings successfully retrieved!',
                            user: user
                        }
                    );
                }
            });
        },

        // Add a user. Only available for Admin use.
        add_user: function(req, res) {

            // Get if user is admin from the token.
            let isAdmin:string = req.decoded._doc.admin;

            // Check if user is admin.
            if (!isAdmin) {
                res.status(403).json({success: false, msg: 'User is not administrator!'});
                return;
            }

            // Check body parameters.
            req.checkBody('username', "Username can not be empty or undefined!").notEmpty();
            req.checkBody('password', "Password can not be empty or undefined!").notEmpty();
            req.checkBody('admin', "Admin should be a boolean!").isBoolean();
            req.checkBody('calories_expected_per_day', "Calories expected per day should be an integer!").isInt();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get username and password from body.
            let username:string = req.body.username;
            let password:string = req.body.password;
            let admin:boolean = req.body.admin;
            let calories_expected_per_day:number = req.body.calories_expected_per_day;

            // Check if the user name is not already used.
            User.findOne({username: username}, function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Add user failed. Server error!"});
                    return;
                }

                if (user) {
                    res.status(404).json({success: false, msg: 'Username already used!'});
                    return;
                }
                else {

                    bcrypt.hash(password, 10, function(err, hash) {

                        if (err) {
                            res.status(500).json({success: false, msg: "Sign up failed. Server error!"});
                            return;
                        }

                        let user = new User({
                            username: username,
                            password: hash,
                            admin: admin,
                            calories_expected_per_day: calories_expected_per_day
                        });

                        // Save the new user.
                        user.save(function (err) {

                            if (err) {
                                res.status(500).json({success: false, msg: 'Add user failed. Server error!'});
                                return;
                            }

                            // Removing password to avoid sending it.
                            let temp:any = user.toObject();
                            delete temp.password;

                            res.status(200).json({success: true, msg: 'User successfully saved!', user: temp});
                        });
                    });
                }
            });
        },

        // Update a user.
        update_user: function(req, res) {

            // Get user from token.
            let username:string = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to apply action.
            var username_to_apply_action:string = req.params.username;

            if (username !== username_to_apply_action) {

                // Check if username is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }
            }
    
            // Check body parameters.
            req.checkBody('calories_expected_per_day', "Calories expected per day should be an integer!").isInt();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get new settings.
            let calories_expected_per_day:string = req.body.calories_expected_per_day;

            // Find the user.
            User.findOne({username: username_to_apply_action}, function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Change settings failed. Server error!"});
                    return;
                }

                if (!user) {
                    res.status(404).json({success: false, msg: 'User not found!'});
                    return;
                }
                else {
                    // Change the calories expected per day.
                    user.calories_expected_per_day = calories_expected_per_day;
                    user.save(function(err) {
                        if (err) {
                            res.status(500).json({success: false, msg: "Change settings failed. Server error!"});
                            return;
                        }

                        res.status(200).json({success: true, msg: 'User successfully updated!', user: user});
                    });
                }
            });
        },

        // Change user password.
        change_user_password: function(req, res) {

            // Get user from token.
            let username:string = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to apply action.
            let username_to_apply_action:string = req.params.username;

            // Check body parameters.
            req.checkBody('new_password', "New password can not be empty or undefined!").notEmpty();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get new password from the body.
            var new_password:string = req.body.new_password;

            if (username !== username_to_apply_action) {

                // Check if user is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }

                // Find the user.
                User.findOne({username: username_to_apply_action}, function (err, user) {

                    if (err) {
                        res.status(500).json({success: false, msg: "Change password failed. Server error!"});
                        return;
                    }

                    if (!user) {
                        res.status(404).json({success: false, msg: 'User not found!'});
                        return;
                    }
                    else {
                        // Change the password.
                        bcrypt.hash(new_password, 10, function(err, hash) {

                            if (err) {
                                res.status(500).json({success: false, msg: "Change password failed. Server error!"});
                                return;
                            }

                            user.password = hash;
                            user.save();

                            res.status(200).json({success: true, msg: "User's password successfully changed!"});
                        });
                    }
                });
            }
            else {
                // Check additional body parameters.
                req.checkBody('current_password', "Current password can not be empty or undefined!").notEmpty();

                let errors:string = req.validationErrors();
                if (errors) {
                    res.status(400).json({success: false, errors: errors});
                    return;
                }

                // Get current password from body.
                let current_password:string = req.body.current_password;

                // Find the user.
                User.findOne({username: username}).select('+password').exec(function (err, user) {

                    if (err) {
                        res.status(500).json({success: false, msg: "Change password failed. Server error!"});
                        return;
                    }

                    if (!user) {
                        res.status(404).json({success: false, msg: 'User not found!'});
                        return;
                    }
                    else {
                        bcrypt.compare(current_password, user.password, function(err, password_match) {

                            // Check if current password is right.
                            if (password_match == false) {
                                res.status(404).json({success: false, msg: 'Wrong password!'});
                            }
                            else {
                                // Change the password.
                                bcrypt.hash(new_password, 10, function(err, hash) {

                                    if (err) {
                                        res.status(500).json({success: false, msg: "Change password failed. Server error!"});
                                        return;
                                    }

                                    user.password = hash;
                                    user.save();

                                    res.status(200).json({success: true, msg:"User's password successfully changed!"});
                                });
                            }


                        });


                    }
                });
            }
        },

        // Remove a user. Only available for Admin use
        delete_user: function(req, res) {

            // Get if user is admin from the token.
            var isAdmin = req.decoded._doc.admin;

            // Check if user is admin.
            if (!isAdmin) {
                res.status(403).json({success: false, msg: 'User is not administrator!'});
                return;
            }

            // Get username to apply action.
            var username_to_apply_action = req.params.username;

            if (username_to_apply_action == 'admin') {
                res.status(403).json({success: false, msg: 'User admin can not be deleted!'});
                return;
            }

            User.findOne({username: username_to_apply_action}, function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Delete user failed. Server error!"});
                    return;
                }

                if (!user) {
                    res.status(404).json({success: false, msg: 'User not found!'});
                    return;
                }
                else {
                    user.remove();

                    // Find the meal.
                    Meal.find({username: username_to_apply_action}, function (err, meals) {
                        if (err) {
                            res.status(500).json({success: false, msg: "Delete meals of user failed. Server error!"});
                            return;
                        }

                        // Removing user's meals.
                        for (let i = 0; i < meals.length; i++) {
                            meals[i].remove();
                        }

                        return res.status(200).json({success: true, msg: 'User successfully deleted!'});
                    });
                }
            });

        }
    }
};