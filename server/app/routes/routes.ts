/// <reference path='../../typings/node/node.d.ts' />

let express = require('express');

let api_users_basic = require('./api_users_basic');
let middleware = require('./middleware');
let api_users = require('./api_users')();
let api_meals = require('./api_meals')();

module.exports.user_basic = function(app) {
    // APIs routes.
    let api = express.Router();

    // Sign up.
    api.post('/signup', api_users_basic(app).signup);

    // Log in.
    api.post('/login', api_users_basic(app).login);

    return api;
};

module.exports.authentication = function(app) {
    // authentication routes.
    let authentication = express.Router();

    // Authentication.
    authentication.use(middleware(app).authentication);

    return authentication;
};

module.exports.user = function() {
    // APIs routes.
    let api = express.Router();

    // Get user list. Only available for Admin user.
    api.get('/users', api_users.get_users);

    // Get settings.
    api.get('/user/:username', api_users.get_user);

    // Add a user. Only available for Admin use.
    api.post('/user/add', api_users.add_user);

    // Change settings.
    api.put('/user/update/:username', api_users.update_user);

    // Change password.
    api.put('/user/change-password/:username', api_users.change_user_password);

    // Remove a user. Only available for Admin use
    api.delete('/user/delete/:username', api_users.delete_user);

    return api;
};

module.exports.meal = function() {
    // APIs routes.
    let api = express.Router();

    // Get meals.
    api.get('/meals/:username', api_meals.meals);

    // Add a new meal.
    api.post('/meal/add/:username', api_meals.add_meal);

    // Update a meal.
    api.put('/meal/update/:username/:meal_id', api_meals.update_meal);

    // Delete a meal.
    api.delete('/meal/delete/:username/:meal_id', api_meals.delete_meal);

    return api;
};