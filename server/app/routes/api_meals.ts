/// <reference path='../../typings/node/node.d.ts' />

let User = require('./../model/user');
let Meal = require('./../model/meal');

module.exports = function() {

    return {
        // Get meals.
        meals: function (req, res) {

            // Get username from token.
            let username:string = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to return its info.
            var username_to_apply_action:string = req.params.username;

            if (username !== username_to_apply_action) {

                // Check if username is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }

                // Check if the user exists.
                User.findOne({username: username_to_apply_action}, function (err, user) {

                    if (err) {
                        res.status(500).json({success: false, msg: "Get settings failed. Server error!"});
                        return;
                    }

                    if (!user) {
                        res.status(404).json({success: false, msg: 'User not found!'});
                        return;
                    }
                    else {
                        Meal.find({username: username_to_apply_action}, function (err, meals) {
                            if (err) {
                                res.status(500).json({success: false, msg: "Getting meals failed. Server error!"});
                                return;
                            }

                            return res.status(200).json({success: true, msg: "user's meals were successfully retrieved!", meals: meals});
                        });
                    }
                });
            }
            else {
                Meal.find({username: username_to_apply_action}, function (err, meals) {
                    if (err) {
                        res.status(500).json({success: false, msg: "Getting meals failed. Server error!"});
                        return;
                    }

                    return res.status(200).json({
                        success: true,
                        msg: "user's meals were successfully retrieved!",
                        meals: meals
                    });
                });
            }
        },

        // Add a new meal.
        add_meal: function (req, res) {

            // Get username from token.
            let username:string = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to apply action.
            var username_to_apply_action:string = req.params.username;

            if (username !== username_to_apply_action) {

                // Check if username is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }
            }

            // Check body parameters.
            req.checkBody('datetime', "Datetime should be a datetime!").isDate();
            req.checkBody('description', "Description can not be empty or undefined!").notEmpty();
            req.checkBody('calories', "Calories Should be an integer number!").isInt();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get meal data from body.
            let datetime:string = req.body.datetime;
            let description:string = req.body.description;
            let calories:number = req.body.calories;

            let meal = new Meal({
                username: username_to_apply_action,
                datetime: datetime,
                description: description,
                calories: calories
            });

            // Check if the user exists.
            User.findOne({username: username_to_apply_action}, function (err, user) {

                if (err) {
                    res.status(500).json({success: false, msg: "Get settings failed. Server error!"});
                    return;
                }

                if (!user) {
                    res.status(404).json({success: false, msg: 'User not found!'});
                    return;
                }
                else {
                    // Save the new meal.
                    meal.save(function (err) {

                        if (err) {
                            res.status(500).json({success: false, msg: "Add meal failed. Server error!"});
                            return;
                        }

                        res.status(200).json({success: true, msg: 'Meal successfully saved!', meal: meal});
                    });
                }
            });

        },

        // Update a meal.
        update_meal: function (req, res) {

            // Get username from token.
            let username = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to apply action.
            var username_to_apply_action:string = req.params.username;

            if (username !== username_to_apply_action) {

                // Check if username is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }
            }

            // Check body parameters.
            req.checkParams('meal_id', "meal_id should be a valid meal id!").isMealId();
            req.checkBody('datetime', "Datetime should be a datetime!").isDate();
            req.checkBody('description', "Description can not be empty or undefined!").notEmpty();
            req.checkBody('calories', "Calories Should be an integer number!").isInt();

            let errors:string = req.validationErrors();
            if (errors) {
                res.status(400).json({success: false, errors: errors});
                return;
            }

            // Get meal data from body.
            let meal_id:string = req.params.meal_id;
            let datetime:string = req.body.datetime;
            let description:string = req.body.description;
            let calories:string = req.body.calories;

            Meal.findOne({_id: meal_id, username: username_to_apply_action}, function (err, meal) {

                if (err) {
                    res.status(500).json({success: false, msg: "Update meal failed. Server error!"});
                    return;
                }

                if (!meal) {
                    res.status(404).json({success: false, msg: 'Meal not found!'});
                    return;
                }
                else {
                    meal.datetime = datetime;
                    meal.description = description;
                    meal.calories = calories;

                    // Save the updated meal.
                    meal.save(function (err) {

                        if (err) {
                            res.status(500).json({success: false, msg: "Update meal failed. Server error!"});
                            return;
                        }

                        res.status(200).json({success: true, msg: 'Meal successfully updated!', meal: meal});
                    });
                }
            });
        },

        // Delete a meal.
        delete_meal: function (req, res) {

            // Get username from token.
            let username = req.decoded._doc.username;
            let isAdmin:boolean = req.decoded._doc.admin;

            // Get username to apply action.
            var username_to_apply_action:string = req.params.username;

            if (username !== username_to_apply_action) {

                // Check if username is admin.
                if (!isAdmin) {
                    res.status(403).json({success: false, msg: 'User is not administrator!'});
                    return;
                }
            }

            // Get meal id from body.
            let meal_id:string = req.params.meal_id;

            // Find the meal.
            Meal.findById({_id: meal_id, username: username_to_apply_action}, function (err, meal) {
                if (err) {
                    res.status(500).json({success: false, msg: "Delete meal failed. Server error!"});
                    return;
                }

                if (!meal) {
                    res.status(404).json({success: false, msg: 'Meal not found!'});
                    return;
                }
                else {
                    meal.remove();
                    return res.status(200).json({success: true, msg: 'Meal successfully deleted!'});
                }
            });
        }
    }
};