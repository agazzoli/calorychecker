/// <reference path='../../typings/node/node.d.ts' />

// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('User', new Schema({
    username: String,
    password: {type: String, select: false},
    admin: Boolean,
    calories_expected_per_day: Number,
    __v: {type: Number, select: false}
}));

