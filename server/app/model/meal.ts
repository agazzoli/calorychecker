/// <reference path='../../typings/node/node.d.ts' />

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Meal', new Schema({
    datetime: Date,
    description: String,
    calories: Number,
    username: String,
    __v: {type: Number, select: false}
}));

