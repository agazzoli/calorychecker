'use strict';

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            js: ["app/*.js", "app/model/*.js", "app/routes/*.js", "app/initial_data/*.js", "helpers/*.js", "test/*.js", "test/helpers/*.js"],
            dts: ["app/*.d.ts", "app/model/*.d.ts", "app/routes/*.d.ts", "app/initial_data/*.d.ts", "helpers/*.d.ts", "test/*.d.ts", "test/helpers/*.d.ts"],
            jsmap: ["app/*.js.map", "app/model/*.js.map", "app/routes/*.js.map", "app/initial_data/*.js.map", "helpers/*.js.map", "test/*.js.map", "test/helpers/*.js.map"]
        },

        ts: {
            default : {
                options : {
                    fast: "never",
                    module: "commonjs",
                    target: "es5",
                    declaration: true
                },
                files: [
                    { src: ["app/server.ts"]},
                    { src: ["app/config.ts"]},
                    { src: ["helpers/init_db.ts"]},
                    { src: ["app/initial_data/load_initial_data.ts"]},
                    { src: ["app/model/user.ts"]},
                    { src: ["app/model/meal.ts"]},
                    { src: ["app/routes/api_meals.ts"]},
                    { src: ["app/routes/api_users.ts"]},
                    { src: ["app/routes/api_users_basic.ts"]},
                    { src: ["app/routes/middleware.ts"]},
                    { src: ["app/routes/routes.ts"]},
                    { src: ["test/helpers/checks.ts"]},
                    { src: ["test/api_users.ts"]},
                    { src: ["test/api_user.ts"]},
                    { src: ["test/api_user-add.ts"]},
                    { src: ["test/api_user-change-password.ts"]},
                    { src: ["test/api_user-update.ts"]},
                    { src: ["test/api_user-delete.ts"]},
                    { src: ["test/api_login.ts"]},
                    { src: ["test/api_signup.ts"]},
                    { src: ["test/api_meals.ts"]},
                    { src: ["test/api_meal-add.ts"]},
                    { src: ["test/api_meal-update.ts"]},
                    { src: ["test/api_meal-delete.ts"]},
                    { src: ["test/static.ts"]}
                ]
            }
        },

        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    quiet: false, // Optionally suppress output to standard out (defaults to false)
                    clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
                },
                src: ['test/*.js']
            }
        },

        execute: {
            initial_data_dev: {
                src: ['app/initial_data/load_initial_data.js'],
                options: {
                    args: ['development']
                }
            },
            initial_data_test: {
                src: ['app/initial_data/load_initial_data.js'],
                options: {
                    args: ['test']
                }
            },
            initial_data_pro: {
                src: ['app/initial_data/load_initial_data.js'],
                options: {
                    args: ['production']
                }
            },
            server_dev: {
                src: ['app/server.js']
            },
            server_test: {
                src: ['app/server.js'],
                options: {
                    args: ['test']
                }
            },
            server_pro: {
                src: ['app/server.js'],
                options: {
                    args: ['production']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-execute');

    grunt.registerTask("default", ["clean", "ts", "mochaTest", "execute:server_dev"]);

    grunt.registerTask("clean-files", ["clean"]);
    grunt.registerTask("compile", ["clean", "ts"]);
    
    grunt.registerTask("test", ["clean", "ts", "mochaTest"]);
    
    grunt.registerTask("start_dev", ["clean", "ts", "execute:server_dev"]);
    grunt.registerTask("start_test", ["clean", "ts", "execute:server_test"]);
    grunt.registerTask("start_pro", ["clean", "ts", "execute:server_pro"]);

    grunt.registerTask("initial_data_dev", ["clean", "ts", "execute:initial_data_dev"]);
    grunt.registerTask("initial_data_test", ["clean", "ts", "execute:initial_data_test"]);
    grunt.registerTask("initial_data_pro", ["clean", "ts", "execute:initial_data_pro"]);
};
