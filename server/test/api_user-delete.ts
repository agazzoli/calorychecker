/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/user/delete:/username", function() {

    let admin_token:string;
    let token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {

            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 0})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 0);
                                    done();
                                });
                        });
                });
        });
    });

    it('01 - should not delete an user and return an error when there is no token', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/andres')
            .end(function(err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not delete an user and return an error when there is an invalid token', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/andres')
            .set('authentication', 'invalid token!')
            .end(function(err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not delete an user and return an error when the user is not administrator', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/andres')
            .set('authentication', token)
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('04 - should not delete admin user and return an error', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/admin')
            .set('authentication', admin_token)
            .end(function(err, res) {
                checks.admin_user_can_not_be_deleted(res);
                done();
            });
    });

    it('05 - should not delete an user and return an error when the user is not found', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/joaquin')
            .set('authentication', admin_token)
            .end(function(err, res) {
                checks.user_not_found(res);
                done();
            });
    });
    
    it('06 - should delete an user', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.user_meal_successfully_saved(res, '2015-08-07T10:19:00.000Z', 'salad', 200, 'andres');

                chai.request(server_url)
                    .delete('/api/user/delete/andres')
                    .set('authentication', admin_token)
                    .end(function(err, res) {
                        checks.user_successfully_deleted(res);

                        // This is just to check that the user was successfully deleted.
                        chai.request(server_url)
                            .get('/api/users')
                            .set({'authentication': admin_token})
                            .end(function (err, res) {
                                checks.users_successfully_retrieved(res);
                                res.body.users[0].should.have.property('username');
                                res.body.users[0].username.should.equal('admin');
                                res.body.users[0].should.have.property('admin');
                                res.body.users[0].admin.should.equal(true);
                                res.body.users.length.should.equal(1);

                                // This is just to check that the meals were successfully deleted.
                                chai.request(server_url)
                                    .get('/api/meals/andres')
                                    .set({'authentication': token})
                                    .end(function (err, res) {
                                        checks.user_meals_successfully_retrieved(res);
                                        res.body.meals.length.should.equal(0);
                                        done();
                                    });
                            });
                    });
            });
    });
});
