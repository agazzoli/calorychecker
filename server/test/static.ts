/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server
let server = require('../app/server');
let config = require('../app/config');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Tests.
describe('/', function() {

    it('01 - should return status 200 and be an html', function (done) {
        chai.request(server_url)
            .get('/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.html;
                done();
            });
    });
});
