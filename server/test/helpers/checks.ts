/// <reference path='../../typings/node/node.d.ts' />
///<reference path="../../typings/chai/chai.d.ts"/>

module.exports = {

    request_ok: function(res:any, msg:string) {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(true);
        res.body.should.have.property('msg');
        res.body.msg.should.equal(msg);
    },
    
    user_ok: function(res:any, username:string, admin:boolean, calories_expected_per_day:number) {
        res.body.should.have.property('user');
        res.body.user.should.be.a('object');
        res.body.user.should.have.property('_id');
        res.body.user.should.have.property('username');
        res.body.user.username.should.equal(username);
        res.body.user.should.have.property('admin');
        res.body.user.admin.should.equal(admin);
        res.body.user.should.have.property('calories_expected_per_day');
        res.body.user.calories_expected_per_day.should.equal(calories_expected_per_day);
        res.body.user.should.not.have.property('password');
    },

    user_successfully_logged_in: function(res:any, username:string, admin:boolean, calories_expected_per_day:number) {
        this.request_ok(res, 'User successfully logged in!');
        res.body.should.have.property('token');
        this.user_ok(res, username, admin, calories_expected_per_day);

        return res.body.token;
    },

    user_successfully_signed_up: function(res:any) {
        this.request_ok(res, 'User successfully signed up!');
    },

    user_successfully_saved: function(res:any, username:string, admin:boolean, calories_expected_per_day:number) {
        this.request_ok(res, 'User successfully saved!');
        this.user_ok(res, username, admin, calories_expected_per_day);
    },

    user_successfully_updated: function(res:any, username:string, admin:boolean, calories_expected_per_day:number) {
        this.request_ok(res, 'User successfully updated!');
        this.user_ok(res, username, admin, calories_expected_per_day);
    },

    user_password_successfully_changed: function(res:any, username:string, admin:boolean, calories_expected_per_day:number) {
        this.request_ok(res, "User's password successfully changed!");
    },

    user_successfully_deleted: function (res:any) {
        this.request_ok(res, 'User successfully deleted!');
    },

    user_settings_successfully_retrieved: function(res:any, username:string, admin:boolean, calories_expected_per_day:number) {
        this.request_ok(res, 'User settings successfully retrieved!');
        this.user_ok(res, username, admin, calories_expected_per_day);
    },

    users_successfully_retrieved: function(res:any) {
        this.request_ok(res, 'Users successfully retrieved!');
        res.body.should.have.property('users');
    },

    meal_ok: function(res:any, datetime:string, description:string, calories:number, username:string) {
        res.body.should.have.property('meal');
        res.body.user.should.be.a('object');
        res.body.user.should.have.property('_id');
        res.body.user.should.have.property('datetime');
        res.body.user.datetime.should.equal(datetime);
        res.body.user.should.have.property('description');
        res.body.user.description.should.equal(description);
        res.body.user.should.have.property('calories');
        res.body.user.description.should.equal(calories);
        res.body.user.should.have.property('username');
        res.body.user.username.should.equal(username);
    },

    user_meals_successfully_retrieved: function(res:any) {
        this.request_ok(res, "user's meals were successfully retrieved!");
        res.body.should.have.property('meals');
    },

    user_meal_successfully_saved: function(res:any, datetime:string, description:string, calories:number, username:string) {
        this.request_ok(res, 'Meal successfully saved!');
    },

    user_meal_successfully_updated: function(res:any, datetime:string, description:string, calories:number, username:string) {
        this.request_ok(res, 'Meal successfully updated!');
    },

    user_meal_successfully_deleted: function(res:any) {
        this.request_ok(res, 'Meal successfully deleted!');
    },

    //------------------------------------------------------
    validation_errors: function(res:any, msgs:string[]) {
        res.should.have.status(400);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(false);
        res.body.should.have.property('errors');

        for (let i = 0; i < res.body.errors.length; i++)
            res.body.errors[i].msg.should.equal(msgs[i]);
    },

    validation_error: function (res:any, msg:string) {
        this.validation_errors(res, [msg]);
    },

    new_password_empty_or_undefined: function (res:any) {
        this.validation_error(res, 'New password can not be empty or undefined!');
    },

    current_password_empty_or_undefined: function (res:any) {
        this.validation_error(res, 'Current password can not be empty or undefined!');
    },

    username_empty_or_undefined: function(res:any) {
        this.validation_error(res, 'Username can not be empty or undefined!');
    },

    password_empty_or_undefined: function(res:any) {
        this.validation_error(res, 'Password can not be empty or undefined!');
    },

    calories_expected_per_day_should_be_an_integer: function(res:any) {
        this.validation_error(res, 'Calories expected per day should be an integer!');
    },

    user_to_apply_action_empty_or_undefined: function(res:any) {
        this.validation_error(res, 'Username to apply action can not be empty or undefined!');
    },

    admin_should_be_a_boolean: function(res:any) {
        this.validation_error(res, 'Admin should be a boolean!');
    },

    datetime_should_be_a_datetime: function(res:any) {
        this.validation_error(res, 'Datetime should be a datetime!');
    },

    description_empty_or_undefined: function(res:any) {
        this.validation_error(res, 'Description can not be empty or undefined!');
    },

    calories_should_be_an_integer: function(res:any) {
        this.validation_error(res, 'Calories Should be an integer number!');
    },

    meal_id_should_be_a_valid_meal_id: function(res:any) {
        this.validation_error(res, 'meal_id should be a valid meal id!');
    },

    //------------------------------------------------------
    request_forbidden: function(res:any, msg:string) {
        res.should.have.status(403);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(false);
        res.body.should.have.property('msg');
        res.body.msg.should.equal(msg);
    },

    no_token_provided: function (res:any) {
        this.request_forbidden(res, 'No token provided!');
    },

    failed_to_authenticate_token: function (res:any) {
        this.request_forbidden(res, 'Failed to authenticate token!');
    },

    user_is_not_administrator: function(res:any) {
        this.request_forbidden(res, 'User is not administrator!');
    },

    admin_user_can_not_be_deleted: function(res:any) {
        this.request_forbidden(res, 'User admin can not be deleted!');
    },

    //------------------------------------------------------
    msg_error: function(res:any, msg:string) {
        res.should.have.status(404);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(false);
        res.body.should.have.property('msg');
        res.body.msg.should.equal(msg);
    },

    user_not_found: function (res:any) {
        this.msg_error(res, 'User not found!');
    },

    wrong_password: function(res:any) {
        this.msg_error(res, 'Wrong password!');
    },

    wrong_user_or_password: function(res:any) {
        this.msg_error(res, 'User or password wrong!');
    },

    username_already_used: function(res:any) {
        this.msg_error(res, 'Username already used!');
    },

    user_meal_not_found: function (res:any) {
        this.msg_error(res, 'Meal not found!');
    },
};