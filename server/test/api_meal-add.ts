/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/meal/add/:username", function() {

    let token:string;
    let admin_token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {

            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 2000})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 2000);
                                    done();
                                });
                        });
                });
        });
    });

    it('01 - should not add an user meal and return an error when there is no token', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not add an user meal and return an error when there is an invalid token', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andreds')
            .set('authentication', 'invalid token!')
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not add an user meal and return an error when there is no datetime', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.datetime_should_be_a_datetime(res);
                done();
            });
    });

    it('04 - should not add an user meal and return an error when datetime is not a datetime', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': 'Hey I am not a datetime!', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.datetime_should_be_a_datetime(res);
                done();
            });
    });

    it('05 - should not add an user meal and return an error when there is no description', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'calories': 200})
            .end(function(err, res) {
                checks.description_empty_or_undefined(res);
                done();
            });
    });

    it('06 - should not add an user meal and return an error when there is no description', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': '', 'calories': 200})
            .end(function(err, res) {
                checks.description_empty_or_undefined(res);
                done();
            });
    });

    it('07 - should not add an user meal and return an error when there is no calories', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad'})
            .end(function(err, res) {
                checks.calories_should_be_an_integer(res);
                done();
            });
    });

    it('08 - should not add an user meal and return an error when calories is not an integer', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 'Hey I am not an integer!'})
            .end(function(err, res) {
                checks.calories_should_be_an_integer(res);
                done();
            });
    });

    it('09 - should not add an user meal and return several errors when several parameters are missing', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .end(function(err, res){
                checks.validation_errors(res, ['Datetime should be a datetime!',
                    'Description can not be empty or undefined!',
                    'Calories Should be an integer number!']);
                done();
            });
    });

    it('10 - should add an user meal', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.user_meal_successfully_saved(res, '2015-08-07T10:19:00.000Z', 'salad', 200, 'andres');

                chai.request(server_url)
                    .get('/api/meals/andres')
                    .set({'authentication': token})
                    .end(function(err, res) {
                        checks.user_meals_successfully_retrieved(res);
                        res.body.meals[0].should.have.property('datetime');
                        res.body.meals[0].datetime.should.equal('2015-08-07T10:19:00.000Z');
                        res.body.meals[0].should.have.property('description');
                        res.body.meals[0].description.should.equal('salad');
                        res.body.meals[0].should.have.property('calories');
                        res.body.meals[0].calories.should.equal(200);

                        res.body.meals.length.should.equal(1);
                        done();
                    });
            });
    });

    it('11 - should not an an user meal to an user and return an error when the user is not administrator', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/joaquin')
            .set('authentication', token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('12 - should not add an user meal to an user and return an error when the user is administrator but the user to add the meal is not found', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/joaquin')
            .set('authentication', admin_token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.user_not_found(res);
                done();
            });
    });

    it('13 - should add an user meal to an user when the user is administrator', function(done) {
        chai.request(server_url)
            .post('/api/meal/add/andres')
            .set('authentication', admin_token)
            .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'salad', 'calories': 200})
            .end(function(err, res) {
                checks.user_meal_successfully_saved(res, '2015-08-07T10:19:00.000Z', 'salad', 200, 'andres');

                chai.request(server_url)
                    .get('/api/meals/andres')
                    .set({'authentication': token})
                    .end(function(err, res) {
                        checks.user_meals_successfully_retrieved(res);
                        res.body.meals[0].should.have.property('datetime');
                        res.body.meals[0].datetime.should.equal('2015-08-07T10:19:00.000Z');
                        res.body.meals[0].should.have.property('description');
                        res.body.meals[0].description.should.equal('salad');
                        res.body.meals[0].should.have.property('calories');
                        res.body.meals[0].calories.should.equal(200);

                        res.body.meals.length.should.equal(1);
                        done();
                    });
            });
    });
    
});
