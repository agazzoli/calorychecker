/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
    config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/user/:username", function() {

    let admin_token:string;
    let token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {
            
            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 1900})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 1900);
                                    done();
                                });
                        });
                });
        });
    });

    it('01 - should not return user info and return an error when there is no token', function (done) {
        chai.request(server_url)
            .put('/api/user/andres')
            .send({})
            .end(function (err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not return user info and return an error when there is an invalid token', function (done) {
        chai.request(server_url)
            .put('/api/user/andres')
            .set('authentication', 'invalid token!')
            .end(function (err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not return user info and return an error when the user is not found', function (done) {
        chai.request(server_url)
            .delete('/api/user/delete/andres')
            .set('authentication', admin_token)
            .end(function (err, res) {
                checks.user_successfully_deleted(res);

                chai.request(server_url)
                    .get('/api/user/andres')
                    .set('authentication', token)
                    .end(function (err, res) {
                        checks.user_not_found(res);
                        done();
                    });
            });
    });

    it('04 - should return user info from a valid user', function (done) {
        chai.request(server_url)
            .get('/api/user/andres')
            .set('authentication', token)
            .end(function (err, res) {
                checks.user_settings_successfully_retrieved(res, 'andres', false, 1900);
                done();
            });
    });

    it('05 - should not return other user info and return an error when the user is not administrator', function(done) {
        chai.request(server_url)
            .get('/api/user/joaquin')
            .set('authentication', token)
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('06 - should return other user info from a valid user when the user is administrator', function (done) {
        chai.request(server_url)
            .get('/api/user/andres')
            .set('authentication', admin_token)
            .end(function (err, res) {
                checks.user_settings_successfully_retrieved(res, 'andres', false, 1900);
                done();
            });
    });
});