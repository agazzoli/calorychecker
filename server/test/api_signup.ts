/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');
let mongoose = require("mongoose");

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe('/api/signup', function() {

    let admin_token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {
            done()
        });
    });

    it('01 - should not add a new user and return an error when there is no username', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'password': 'password', 'calories_expected_per_day':0})
            .end(function(err, res) {
                checks.username_empty_or_undefined(res);
                done();
            });
    });

    it('02 - should not add a new user and return an error when username is empty', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': '', 'password': 'password', 'calories_expected_per_day':0})
            .end(function(err, res) {
                checks.username_empty_or_undefined(res);
                done();
            });
    });

    it('03 - should not add a new user and return an error when there is no password', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'calories_expected_per_day':0})
            .end(function(err, res) {
                checks.password_empty_or_undefined(res);
                done();
            });
    });

    it('04 - should not add a new user and return an error when password is empty', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password': '', 'calories_expected_per_day':0})
            .end(function(err, res) {
                checks.password_empty_or_undefined(res);
                done();
            });
    });

    it('05 - should not add a new user and return an error when there is no calories expected per day', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password':'password'})
            .end(function(err, res) {
                checks.calories_expected_per_day_should_be_an_integer(res);
                done();
            });
    });

    it('06 - should not add a new user and return an error when calories expected per day is not an integer', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password':'password', 'calories_expected_per_day': 'hey i am not an integer'})
            .end(function(err, res) {
                checks.calories_expected_per_day_should_be_an_integer(res);
                done();
            });
    });

    it('07 - should not add a new user and return several errors when there is no username, password or calories expected per day', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({})
            .end(function(err, res) {
                checks.validation_errors(res, ['Username can not be empty or undefined!',
                                                'Password can not be empty or undefined!',
                                                'Calories expected per day should be an integer!']);
                done();
            });
    });

    it('08 - should not add a new user and return several errors when username and password are empties and calories expected per day is not an integer', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({})
            .end(function(err, res) {
                checks.validation_errors(res, ['Username can not be empty or undefined!',
                                                'Password can not be empty or undefined!',
                                                'Calories expected per day should be an integer!']);
                done();
            });
    });

    it('10 - should add a new user', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 2000})
            .end(function(err, res) {
                checks.user_successfully_signed_up(res);

                // This is just to check that the new user was correctly added.
                chai.request(server_url)
                    .post('/api/login')
                    .send({'username': 'andres', 'password': 'password'})
                    .end(function(err, res){
                        let token:string = checks.user_successfully_logged_in(res, 'andres', false, 2000);

                        chai.request(server_url)
                            .get('/api/user/andres')
                            .set({'authentication': token})
                            .end(function (err, res) {
                                checks.user_settings_successfully_retrieved(res, 'andres', false, 2000);
                                done();
                            });
                    });
            });
    });

    it('11 - should not add a duplicated user', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 2000})
            .end(function(err, res) {
                checks.user_successfully_signed_up(res);

                chai.request(server_url)
                    .post('/api/signup')
                    .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 2000})
                    .end(function(err, res){
                        checks.username_already_used(res);
                        done();
                    });
            });
    });

});
