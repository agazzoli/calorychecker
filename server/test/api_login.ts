/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/login", function() {

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() { 
            done() 
        });
    });

    it('01 - should not add a new user and return an error when there is no username', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({'password': 'password'})
            .end(function(err, res) {
                checks.username_empty_or_undefined(res);
                done();
            });
    });

    it('02 - should not add a new user and return an error when username is empty', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({'username': '', 'password': 'password'})
            .end(function(err, res) {
                checks.username_empty_or_undefined(res);
                done();
            });
    });

    it('03 - should not add a new user and return an error when there is no password', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({'username': 'andres'})
            .end(function(err, res) {
                checks.password_empty_or_undefined(res);
                done();
            });
    });

    it('04 - should not add a new user and return an error when password is empty', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({'username': 'andres', 'password': ''})
            .end(function(err, res){
                checks.password_empty_or_undefined(res);
                done();
            });
    });

    it('05 - should not add a new user and return several errors when there is no username or password', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({})
            .end(function(err, res){
                checks.validation_errors(res, ['Username can not be empty or undefined!','Password can not be empty or undefined!']);
                done();
            });
    });

    it('06 - should not add a new user and return several errors when username and password are empties', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({})
            .end(function(err, res){
                checks.validation_errors(res, ['Username can not be empty or undefined!','Password can not be empty or undefined!']);
                done();
            });
    });

    it('07 - should log in a valid user', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 0})
            .end(function(err, res){
                checks.user_successfully_signed_up(res);

                chai.request(server_url)
                    .post('/api/login')
                    .send({'username': 'andres', 'password': 'password'})
                    .end(function(err, res){
                        checks.user_successfully_logged_in(res, 'andres', false, 0);
                        done();
                    });
            });
    });

    it('08 - should not log in an invalid user', function(done) {
        chai.request(server_url)
            .post('/api/login')
            .send({'username': 'andres1', 'password': 'password'})
            .end(function(err, res){
                checks.wrong_user_or_password(res);
                done();
            });
    });

    it('09 - should not log in an user with an invalid password', function(done) {
        chai.request(server_url)
            .post('/api/signup')
            .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 0})
            .end(function(err, res){
                checks.user_successfully_signed_up(res);

                chai.request(server_url)
                    .post('/api/login')
                    .send({'username': 'andres', 'password': 'invalid'})
                    .end(function(err, res){
                        checks.wrong_user_or_password(res);
                        done();
                    });
            });
    });
    
});
