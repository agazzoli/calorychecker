/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');
let mongoose = require("mongoose");

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/user/add", function() {

    let admin_token:string;
    let token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {

            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 0})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 0);
                                    done();
                                });
                        });
                });
        });
    });

    it('01 - should not add an user and return an error when there is no token', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .send({'username': 'joaquin', 'password': 'password', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not add an user and return an error when there is an invalid token', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', 'invalid token!')
            .send({'username': 'joaquin', 'password': 'password', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not add an user and return and error when the user is not administrator', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', token)
            .send({'username': 'joaquin', 'password': 'password', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('04 - should not add an user and return an error when there is no username', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'password': 'password', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.username_empty_or_undefined(res);
                done();
            });
    });

    it('05 - should not add an user and return an error when username is empty', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': '', 'password': 'password', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.username_empty_or_undefined(res);
                done();
            });
    });

    it('06 - should not add an user and return an error when there is no password', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.password_empty_or_undefined(res);
                done();
            });
    });

    it('07 - should not add an user and return an error when password is empty', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'password': '', 'admin': false, 'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.password_empty_or_undefined(res);
                done();
            });
    });

    it('08 - should not add an user and return an error when there is no admin value', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'password': 'password', 'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.admin_should_be_a_boolean(res);
                done();
            });
    });

    it('09 - should not add an user and return an error when admin value is not a boolean', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'password': 'password', 'admin': 'Hey, I am not a boolean!', 'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.admin_should_be_a_boolean(res);
                done();
            });
    });

    it('10 - should not add an user and return an error when there is no calories expected per day', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'password': 'password', 'admin': false})
            .end(function(err, res){
                checks.calories_expected_per_day_should_be_an_integer(res);
                done();
            });
    });

    it('11 - should not add an user and return an error when calories expected per day is not an integer', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'password': 'password', 'admin': false, 'calories_expected_per_day': 'Hey I am not an integer'})
            .end(function(err, res){
                checks.calories_expected_per_day_should_be_an_integer(res);
                done();
            });
    });

    it('12 - should not add an user and return several errors when several parameters are missing', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .end(function(err, res){
                checks.validation_errors(res, ['Username can not be empty or undefined!',
                                                'Password can not be empty or undefined!',
                                                'Admin should be a boolean!',
                                                'Calories expected per day should be an integer!']);
                done();
            });
    });

    it('13 - should not add an user and return an error when the username is already used', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'andres', 'password': 'password', 'admin': false, 'calories_expected_per_day': 0})
            .end(function(err, res) {
                checks.username_already_used(res);
                done();
            });
    });
    
    it('14 - should add an user', function(done) {
        chai.request(server_url)
            .post('/api/user/add')
            .set('authentication', admin_token)
            .send({'username': 'joaquin', 'password': 'password', 'admin': false, 'calories_expected_per_day': 0})
            .end(function(err, res) {
                checks.user_successfully_saved(res, 'joaquin', false, 0);

                // This is just to check that the user was successfully deleted.
                chai.request(server_url)
                    .get('/api/users')
                    .set({'authentication': admin_token})
                    .end(function (err, res) {
                        checks.users_successfully_retrieved(res);
                        res.body.users[0].should.have.property('username');
                        res.body.users[0].username.should.equal('admin');
                        res.body.users[0].should.have.property('admin');
                        res.body.users[0].admin.should.equal(true);

                        res.body.users[1].should.have.property('username');
                        res.body.users[1].username.should.equal('andres');
                        res.body.users[1].should.have.property('admin');
                        res.body.users[1].admin.should.equal(false);

                        res.body.users[2].should.have.property('username');
                        res.body.users[2].username.should.equal('joaquin');
                        res.body.users[2].should.have.property('admin');
                        res.body.users[2].admin.should.equal(false);

                        res.body.users.length.should.equal(3);

                        done();
                    });
            });
    });
});
