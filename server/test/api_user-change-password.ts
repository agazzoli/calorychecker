/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/user/change-password/:username", function() {

    let admin_token:string;
    let token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {

            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 0})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 0);
                                    done();
                                });
                        });
                });
        });
    });

    it('01 - should not change a user password and return an error when there is no token', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not change a user password and return an error when there is an invalid token', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', 'invalid token!')
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not change a user password and return an error when there is no new password', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', token)
            .send({'current_password': 'password'})
            .end(function(err, res){
                checks.new_password_empty_or_undefined(res);
                done();
            });
    });

    it('04 - should not change a user password and return an error when new password is empty', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', token)
            .send({'current_password': 'password', 'new_password': ''})
            .end(function(err, res){
                checks.new_password_empty_or_undefined(res);
                done();
            });
    });

    it('05 - should not change a user password and return an error when there is no current password', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', token)
            .send({'new_password': 'pass123'})
            .end(function(err, res){
                checks.current_password_empty_or_undefined(res);
                done();
            });
    });

    it('06 - should not change a user password and return an error when current password is empty', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', token)
            .send({'current_password': '', 'new_password': 'pass123'})
            .end(function(err, res){
                checks.current_password_empty_or_undefined(res);
                done();
            });
    });

    it('07 - should not change a user password and return an error when the user is not found', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/andres')
            .set('authentication', admin_token)
            .end(function(err, res) {
                checks.user_successfully_deleted(res);

                chai.request(server_url)
                    .put('/api/user/change-password/andres')
                    .send({'current_password': 'password', 'new_password': 'pass123', 'token': token})
                    .end(function(err, res) {
                        checks.user_not_found(res);
                        done();
                    });
            });
    });

    it('08 - should not change a user password and return an error when current password does not match', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', token)
            .send({'current_password': 'password1', 'new_password': 'pass123'})
            .end(function(err, res) {
                checks.wrong_password(res);
                done();
            });
    });
    
    it('09 - should change the password from a valid user', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', token)
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res) {
                checks.user_password_successfully_changed(res);

                // This is just to check that the password was successfully changed.
                chai.request(server_url)
                    .post('/api/login')
                    .set('authentication', token)
                    .send({'username': 'andres', 'password': 'pass123'})
                    .end(function(err, res) {
                        token = checks.user_successfully_logged_in(res, 'andres', false, 0);
                        done();
                    });
            });
    });

    it('10 - should not change other user password and return an error when the user is not administrator', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/joaquin')
            .set('authentication', token)
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('11 - should not change other user password and return an error when the user is administrator but the user to change the password is not found', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/joaquin')
            .set('authentication', admin_token)
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res){
                checks.user_not_found(res);
                done();
            });
    });

    it('12 - should change other user password when the user is administrator', function(done) {
        chai.request(server_url)
            .put('/api/user/change-password/andres')
            .set('authentication', admin_token)
            .send({'current_password': 'password', 'new_password': 'pass123'})
            .end(function(err, res){
                checks.user_password_successfully_changed(res);

                // This is just to check that the password was successfully changed.
                chai.request(server_url)
                    .post('/api/login')
                    .send({'username': 'andres', 'password': 'pass123', 'token': admin_token})
                    .end(function(err, res) {
                        token = checks.user_successfully_logged_in(res, 'andres', false, 0);
                        done();
                    });
            });
    });
});
