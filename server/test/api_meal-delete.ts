/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/meal/delete/:username/:meal_id", function() {

    let token:string;
    let admin_token:string;
    let meal_id:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {

            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 2000})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 2000);

                                    chai.request(server_url)
                                        .post('/api/meal/add/andres')
                                        .set('authentication', token)
                                        .send({'datetime': '2015-08-07T10:19:00.000Z', 'description': 'sandwich', 'calories': 400})
                                        .end(function(err, res) {
                                            checks.user_meal_successfully_saved(res, '2015-08-07T10:19:00.000Z', 'sandwich', 400, 'andres');

                                            chai.request(server_url)
                                                .get('/api/meals/andres')
                                                .set({'authentication': token})
                                                .end(function(err, res) {
                                                    checks.user_meals_successfully_retrieved(res);
                                                    res.body.meals[0].should.have.property('datetime');
                                                    res.body.meals[0].datetime.should.equal('2015-08-07T10:19:00.000Z');
                                                    res.body.meals[0].should.have.property('description');
                                                    res.body.meals[0].description.should.equal('sandwich');
                                                    res.body.meals[0].should.have.property('calories');
                                                    res.body.meals[0].calories.should.equal(400);

                                                    res.body.meals.length.should.equal(1);

                                                    meal_id = res.body.meals[0]._id;
                                                    done();
                                                });
                                        });
                                });
                        });
                });
        });
    });

    it('01 - should not delete an user meal and return an error when there is no token', function(done) {
        chai.request(server_url)
            .delete('/api/meal/delete/andres/' + meal_id)
            .end(function(err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not delete an user meal and return an error when there is an invalid token', function(done) {
        chai.request(server_url)
            .delete('/api/meal/delete/andres/' + meal_id)
            .set('authentication', 'invalid token!')
            .end(function(err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not delete an user meal and return an error when the meal is not found', function(done) {
        chai.request(server_url)
            .delete('/api/meal/delete/andres/57426ac6fa6e0c781785c84c')
            .set('authentication', token)
            .end(function(err, res) {
                checks.user_meal_not_found(res);
                done();
            });
    });

    it('04 - should delete an user meal', function(done) {
        chai.request(server_url)
            .delete('/api/meal/delete/andres/' + meal_id)
            .set('authentication', token)
            .end(function(err, res) {
                checks.user_meal_successfully_deleted(res);

                chai.request(server_url)
                    .get('/api/meals/andres')
                    .set({'authentication': token})
                    .end(function(err, res) {
                        checks.user_meals_successfully_retrieved(res);
                        res.body.meals.length.should.equal(0);
                        done();
                    });
            });
    });

    it('05 - should not delete an user meal of an user and return an error when the user is not administrator', function(done) {
        chai.request(server_url)
            .delete('/api/meal/delete/joaquin/' + meal_id)
            .set('authentication', token)
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('06 - should update an user meal of an user when the user is administrator', function(done) {
        chai.request(server_url)
            .delete('/api/meal/delete/andres/' + meal_id)
            .set('authentication', admin_token)
            .end(function(err, res) {
                checks.user_meal_successfully_deleted(res);

                chai.request(server_url)
                    .get('/api/meals/andres')
                    .set({'authentication': token})
                    .end(function(err, res) {
                        checks.user_meals_successfully_retrieved(res);
                        res.body.meals.length.should.equal(0);
                        done();
                    });
            });
    });
    
});
