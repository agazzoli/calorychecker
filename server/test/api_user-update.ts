/// <reference path='../typings/node/node.d.ts' />
/// <reference path='../typings/mocha/mocha.d.ts' />

process.env.NODE_ENV = 'test';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Server.
let server = require('../app/server');
let config = require('../app/config');

// Helpers.
let init_db = require('../helpers/init_db');
let checks = require('./helpers/checks');

// Utils.
let chai = require('chai');
let chaiHttp = require('chai-http');

let should = chai.should();
chai.use(chaiHttp);

// Server url.
let server_url = config.server[process.env.NODE_ENV].protocol + '://' +
                config.server[process.env.NODE_ENV].host + ':' + config.server[process.env.NODE_ENV].port;

// Database.
let database = config.database[process.env.NODE_ENV];

// Tests.
describe("/api/user/update/:username", function() {

    let admin_token:string;
    let token:string;

    // For each test database is dropped.
    beforeEach(function (done) {
        init_db(database, function() {

            // Admin user.
            chai.request(server_url)
                .post('/api/login')
                .send({'username': 'admin', 'password': 'admin'})
                .end(function(err, res) {
                    admin_token = checks.user_successfully_logged_in(res, 'admin', true, 0);

                    // Normal user.
                    chai.request(server_url)
                        .post('/api/signup')
                        .send({'username': 'andres', 'password': 'password', 'calories_expected_per_day': 0})
                        .end(function(err, res) {
                            checks.user_successfully_signed_up(res);

                            chai.request(server_url)
                                .post('/api/login')
                                .send({'username': 'andres', 'password': 'password'})
                                .end(function(err, res) {
                                    token = checks.user_successfully_logged_in(res, 'andres', false, 0);
                                    done();
                                });
                        });
                });
        });
    });

    it('01 - should not change user info and return an error when there is no token', function(done) {
        chai.request(server_url)
            .put('/api/user/update/andres')
            .send({'calories_expected_per_day': 500})
            .end(function(err, res) {
                checks.no_token_provided(res);
                done();
            });
    });

    it('02 - should not change user info and return an error when there is an invalid token', function(done) {
        chai.request(server_url)
            .put('/api/user/update/andres')
            .set('authentication', 'invalid token!')
            .send({'calories_expected_per_day': 500})
            .end(function(err, res) {
                checks.failed_to_authenticate_token(res);
                done();
            });
    });

    it('03 - should not change user info and return an error when there is no calories expected per day', function(done) {
        chai.request(server_url)
            .put('/api/user/update/andres')
            .set('authentication', token)
            .end(function(err, res){
                checks.calories_expected_per_day_should_be_an_integer(res);
                done();
            });
    });

    it('04 - should not change user info and return an error when calories expected per day is not an integer', function(done) {
        chai.request(server_url)
            .put('/api/user/update/andres')
            .set('authentication', token)
            .send({'calories_expected_per_day': 'Hey i am not an integer'})
            .end(function(err, res){
                checks.calories_expected_per_day_should_be_an_integer(res);
                done();
            });
    });

    it('05 - should not change user info and return an error when the user is not found', function(done) {
        chai.request(server_url)
            .delete('/api/user/delete/andres')
            .set('authentication', admin_token)
            .end(function(err, res) {
                checks.user_successfully_deleted(res);

                chai.request(server_url)
                    .put('/api/user/update/andres')
                    .send({'calories_expected_per_day': 500, 'token': token})
                    .end(function(err, res) {
                        checks.user_not_found(res);
                        done();
                    });
            });
    });
    
    it('06 - should change the user info from a valid user', function(done) {
        chai.request(server_url)
            .put('/api/user/update/andres')
            .set('authentication', token)
            .send({'calories_expected_per_day': 500})
            .end(function(err, res) {
                checks.user_successfully_updated(res, 'andres', false, 500);

                // This is just to check that the calories expected per day was successfully changed.
                chai.request(server_url)
                    .get('/api/user/andres')
                    .set({'authentication': token})
                    .end(function (err, res) {
                        checks.user_settings_successfully_retrieved(res, 'andres', false, 500);
                        done();
                    });
            });
    });

    it('07 - should not change other user info and return an error when the user is not administrator', function(done) {
        chai.request(server_url)
            .put('/api/user/update/joaquin')
            .set('authentication', token)
            .send({'calories_expected_per_day': 2000})
            .end(function(err, res) {
                checks.user_is_not_administrator(res);
                done();
            });
    });

    it('08 - should not change other user info and return an error when the user is administrator but the user to update is not found', function(done) {
        chai.request(server_url)
            .put('/api/user/update/joaquin')
            .set('authentication', admin_token)
            .send({'calories_expected_per_day': 2000})
            .end(function(err, res){
                checks.user_not_found(res);
                done();
            });
    });

    it('09 - should change other user info when the user is administrator', function(done) {
        chai.request(server_url)
            .put('/api/user/update/andres')
            .set('authentication', admin_token)
            .send({'calories_expected_per_day': 3000})
            .end(function(err, res){
                checks.user_successfully_updated(res, 'andres', false, 3000);

                // This is just to check that the calories expected per day was successfully changed.
                chai.request(server_url)
                    .get('/api/user/andres')
                    .set({'authentication': token})
                    .end(function (err, res) {
                        checks.user_settings_successfully_retrieved(res, 'andres', false, 3000);
                        done();
                    });
            });
    });

});
