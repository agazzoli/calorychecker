'use strict';

var caloryCheckerServices = angular.module('caloryChecker.services');

caloryCheckerServices.factory('UserBasic', ['$resource',
    function($resource) {
        return function() {
            return $resource('/api', {}, {
                signup: {
                    method: 'POST',
                    url: '/api/signup'
                },
                login: {
                    method: 'POST',
                    url: '/api/login'
                }
            });
        }
    }
]);

caloryCheckerServices.factory('User', ['$resource',
    function($resource) {
        return function(custom_headers) {
            return $resource('/api/users', {}, {
                get_users: {
                    method: 'GET',
                    headers: custom_headers
                },
                get_user: {
                    method: 'GET',
                    url: '/api/user/:username',
                    headers: custom_headers
                },
                add: {
                    method: 'POST',
                    url: '/api/user/add',
                    headers: custom_headers
                },
                update: {
                    method: 'PUT',
                    url: '/api/user/update/:username',
                    headers: custom_headers
                },
                change_password: {
                    method: 'PUT',
                    url: '/api/user/change-password/:username',
                    headers: custom_headers
                },
                delete: {
                    method: 'DELETE',
                    url: '/api/user/delete/:username',
                    headers: custom_headers
                }
            });
        }
    }
]);

caloryCheckerServices.factory('Meal', ['$resource',
    function($resource) {
        return function(custom_headers) {
            return $resource('/api/meals/:username', {}, {
                get_meals: {
                    method: 'GET',
                    headers: custom_headers
                },
                add: {
                    method: 'POST',
                    url: '/api/meal/add/:username',
                    headers: custom_headers
                },
                update: {
                    method: 'PUT',
                    url: '/api/meal/update/:username/:meal_id',
                    headers: custom_headers
                },
                delete: {
                    method: 'DELETE',
                    url: '/api/meal/delete/:username/:meal_id',
                    headers: custom_headers
                }
            });
        }
    }
]);
