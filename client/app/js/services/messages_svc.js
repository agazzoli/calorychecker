'use strict';

var caloryCheckerServices = angular.module('caloryChecker.services');

caloryCheckerServices.factory('Messages', ['$uibModal',
    function($uibModal) {
        return {
            open_dialog: function(data) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'partials/alert.html',
                    controller: alertCtrl,
                    backdrop: true,
                    keyboard: true,
                    backdropClick: true,
                    resolve: {
                        data: function () {
                          return data;
                        }
                    }
                });
            },
            show_error: function(err) {
                var error_text = err.data.msg;

                // This should not happen due web validations.
                if (typeof error_text === 'undefined')
                    if (typeof err.data.errors !== 'undefined' && (err.data.errors.length > 0))
                        error_text = err.data.errors[0].msg;
                    else
                        error_text = 'Error. Unknown';


                var data = {
                    title: "Error: ",
                    text: error_text,
                    mode: 'danger'
                };

                this.open_dialog(data);
            },
            show_message: function(title, message, mode) {
                var data = {
                    title: title,
                    text: message,
                    mode: mode
                };

                this.open_dialog(data);
            }
        }
    }
]);