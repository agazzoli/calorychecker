'use strict';

/* App Module */

var caloryCheckerApp = angular.module('caloryChecker.app', [
    'ngRoute',
    'ngCookies',
    'ngMessages',
    'ngPassword',
    'caloryChecker.filters',
    'caloryChecker.controllers',
    'caloryChecker.services'
]);

caloryCheckerApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/login.html',
                controller: 'LoginCtrl'
            }).
            when('/signup', {
                templateUrl: 'partials/signup.html',
                controller: 'SignupCtrl'
            }).
            when('/users', {
                templateUrl: 'partials/users.html',
                controller: 'UsersCtrl'
            }).
            when('/meals', {
                templateUrl: 'partials/meals.html',
                controller: 'MealsCtrl'
            }).
            when('/meals/:username', {
                templateUrl: 'partials/meals.html',
                controller: 'MealsCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
  }
]);
