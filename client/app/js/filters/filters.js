'use strict';

var caloryCheckerFilters = angular.module('caloryChecker.filters', []);

caloryCheckerFilters.filter("datetimefilter", function() {
    return function(items, dateFrom, dateTo, timeFrom, timeTo) {
        var result = [];

        if (typeof items === 'undefined')
            return result;

        var date_from = null;
        var date_to = null;

        var time_from = null;
        var time_to = null;

        if ((dateFrom !== null) && (typeof dateFrom !== 'undefined'))
            date_from = getOnlyDateFromDatetime(dateFrom);

        if ((dateTo !== null) && (typeof dateTo !== 'undefined'))
            date_to = getOnlyDateFromDatetime(dateTo);

        if ((timeFrom !== null) && (typeof timeFrom !== 'undefined'))
            time_from = getOnlyTimeFromDatetime(timeFrom);

        if ((timeTo !== null) && (typeof timeTo !== 'undefined'))
            time_to = getOnlyTimeFromDatetime(timeTo);

        for (var i = 0; i < items.length; i++) {

            var item = items[i];
            var meal_date = getOnlyDateFromDatetime(new Date(item.datetime));
            var meal_time = getOnlyTimeFromDatetime(new Date(item.datetime));

            if (isInRange(meal_date, date_from, date_to) &&
                isInRange(meal_time, time_from, time_to))
                result.push(item);
        }

        return result;
    };
});