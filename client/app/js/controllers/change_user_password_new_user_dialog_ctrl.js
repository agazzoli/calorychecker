'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('changeUserPasswordDialogCtrl',  ['$scope', '$uibModalInstance', '$cookies', 'User', 'username', 'include_current_password', 'Messages',
    function($scope, $uibModalInstance, $cookies, User, username, include_current_password, Messages) {

        $scope.username = username;
        $scope.include_current_password = include_current_password;
        $scope.current_password = '';
        $scope.new_password = '';
        $scope.new_password2 = '';

        $scope.saveUser = function () {
            var custom_headers = {"authentication": $cookies.get('auth_token')};

            var change_password_data = {
                'current_password': $scope.current_password,
                'new_password': $scope.new_password
            };

            var res_change_user_password = User(custom_headers).change_password({username: $scope.username}, change_password_data,
                function() {
                    $uibModalInstance.close(res_change_user_password.user);
                },
                function(err) {
                    $uibModalInstance.dismiss('cancel');
                    Messages.show_error(err);
                }
            );
        };
        
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);