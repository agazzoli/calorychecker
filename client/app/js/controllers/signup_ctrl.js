'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('SignupCtrl', ['$scope', '$http', '$location', 'UserBasic',
    function($scope, $http, $location, UserBasic) {

        $scope.show_login_error_msg = false;
        $scope.show_new_account_created_msg = false;
        
        $scope.goBackToLogIn = function() {
            $location.path('/');
        };

        $scope.signup = function() {
            var signup_data = {
                'username': $scope.username,
                'password': $scope.password,
                'calories_expected_per_day': $scope.calories_expected_per_day
            };

            var res = UserBasic().signup(signup_data,
                function() {
                    $scope.show_new_account_created_msg = true;
                },
                function(err) {
                    if (typeof err.data.msg !== 'undefined')
                        $scope.error_msg = err.data.msg;
                    else if ((typeof err.data.errors !== 'undefined') && (err.data.errors.length > 0)) // This should not happen due web validations.
                        $scope.error_msg = err.data.errors[0].msg; // I show the first error.

                    $scope.show_signup_error_msg = true;
                }
            );
        };
    }
]);
