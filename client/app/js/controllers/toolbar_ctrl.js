'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('ToolbarCtrl', ['$scope', '$uibModal', '$location', '$routeParams', '$cookies', 'Messages',
    function($scope, $uibModal, $location, $routeParams, $cookies, Messages) {

        var username = $cookies.get('username');
        var calories_expected_per_day = Number($cookies.get('calories_expected_per_day'));

        $scope.username = username;

        $scope.changeUserSettings = function() {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/modify-user-dialog.html',
                controller: 'modifyUserDialogCtrl',
                resolve: {
                    username: function () {
                        return username;
                    },
                    calories_expected_per_day: function () {
                        return calories_expected_per_day;
                    }
                }
            });

            modal.result.then(function(modified_user) {
                $cookies.put('calories_expected_per_day', modified_user.calories_expected_per_day);

                if ($location.path() === '/users') {
                    // Updating user in view.
                    var user = $scope.$parent.$parent.users.find(function(user) {
                        return user.username === modified_user.username;
                    });

                    if (typeof user !== 'undefined')
                        user.calories_expected_per_day = modified_user.calories_expected_per_day;
                }
                else {
                    // Updating meals view just if the meals's user belongs to the user logged in.
                    if ($routeParams.username === modified_user.username)
                        $scope.$parent.$parent.current_user_calories_expected_per_day = modified_user.calories_expected_per_day;
                }
            });
        };

        $scope.changeUserPassword = function() {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/change-user-password-dialog.html',
                controller: 'changeUserPasswordDialogCtrl',
                resolve: {
                    username: function () {
                        return username;
                    },
                    include_current_password: function () {
                        return true;
                    }
                }
            });

            modal.result.then(function() {
                Messages.show_message('Change password:', "User's password was successfully changed", 'success');
            });
        };

        $scope.logOut = function() {
            $location.path('/');

            $cookies.remove('auth_token');
            $cookies.remove('username');
            $cookies.remove('admin');
            $cookies.remove('calories_expected_per_day');
        }
    }
]);
