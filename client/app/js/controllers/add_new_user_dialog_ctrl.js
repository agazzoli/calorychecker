'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('addNewUserDialogCtrl',  ['$scope', '$uibModalInstance', '$cookies', 'User', 'Messages',
    function($scope, $uibModalInstance, $cookies, User, Messages) {

        $scope.username = '';
        $scope.password = '';
        $scope.password2 = '';
        $scope.admin = false;
        $scope.calories_expected_per_day = 0;

        $scope.saveUser = function () {
            var custom_headers = {"authentication": $cookies.get('auth_token')};

            var add_user_data = {
                'username': $scope.username,
                'password': $scope.password,
                'admin': $scope.admin,
                'calories_expected_per_day': $scope.calories_expected_per_day
            };
            
            var res_add_user = User(custom_headers).add(add_user_data,
                function() {
                    $uibModalInstance.close(res_add_user.user);
                },
                function(err) {
                    $uibModalInstance.dismiss('cancel');
                    Messages.show_error(err);
                }
            );
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

