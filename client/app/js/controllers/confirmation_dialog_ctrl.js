'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('confirmationDialogCtrl', ['$scope', '$uibModalInstance', 'data',
    function($scope, $uibModalInstance, data) {

        $scope.data = data;

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);
