'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('modifyMealDialogCtrl',  ['$scope', '$uibModalInstance', '$cookies', 'Meal', 'meal', 'username', 'Messages',
    function($scope, $uibModalInstance, $cookies, Meal, meal, username, Messages) {

        $scope.add_dialog = false;

        $scope.date = new Date(meal.datetime);
        $scope.time = new Date(meal.datetime);
        $scope.description = meal.description;
        $scope.calories = meal.calories;

        $scope.saveMeal = function () {

            var custom_headers = {"authentication": $cookies.get('auth_token')};

            var modify_meal_data = {
                'datetime': getDatetime($scope.date, $scope.time),
                'description': $scope.description,
                'calories': $scope.calories
            };

            Meal(custom_headers).update({username: username, meal_id: meal._id}, modify_meal_data,
                function(res_update_meal) {
                    $uibModalInstance.close(res_update_meal.meal);
                },
                function(err) {
                    $uibModalInstance.dismiss('cancel');
                    Messages.show_error(err);
                }
            );
        };
        
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        
        $scope.openDate = function() {
            $scope.datePopup.opened = true;
        };

        $scope.datePopup = {
            opened: false
        };
    }
]);
