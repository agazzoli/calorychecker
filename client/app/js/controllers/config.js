'use strict';

/* Controllers */
var caloryCheckerControllers = angular.module('caloryChecker.controllers', ['ngMaterial', 
                                                                            'ngCookies', 
                                                                            'ui.bootstrap', 
                                                                            'ngAnimate']);

caloryCheckerControllers.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider.icon("more_vert", 'images/icons/more_vert.svg', 24);
}
]);
    
