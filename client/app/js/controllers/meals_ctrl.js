'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('MealsCtrl', ['$scope', '$routeParams', '$cookies', '$location', 'User', 'Meal', '$uibModal', 'Messages',
    function($scope, $routeParams, $cookies, $location, User, Meal, $uibModal, Messages) {

        var custom_headers = {"authentication": $cookies.get('auth_token')};

        $scope.current_user_calories_expected_per_day = -1;
        $scope.calories_today = 0;

        var username = $routeParams.username;

        Meal(custom_headers).get_meals({username: username},
            function(res) {
                $scope.meals = addFormattedDatetimeToMeals(res.meals);
                $scope.calories_today = getCaloriesToday(res.meals);

                User(custom_headers).get_user({username: username},
                    function(res) {
                        $scope.current_user_calories_expected_per_day = res.user.calories_expected_per_day;
                        $scope.calories_today_style = getCaloriesRowStyle($scope.current_user_calories_expected_per_day, $scope.calories_today);
                    }
                );
            }
        );

        $scope.show_users_in_breadcrumbs = ($cookies.get('admin') == 'true');

        $scope.goBackToUsers = function() {
            $location.path('/users');
        };

        $scope.addNewMeal = function() {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/add-and-modify-meal-dialog.html',
                controller: 'addNewMealDialogCtrl',
                resolve: {
                    username: function () {
                        return username;
                    }
                }
            });

            modal.result.then(function(added_meal) {
                $scope.meals.push(addFormattedDatetime(added_meal));
            });
        };

        $scope.modifyMeal = function(meal) {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/add-and-modify-meal-dialog.html',
                controller: 'modifyMealDialogCtrl',
                resolve: {
                    username: function () {
                        return username;
                    },
                    meal: function () {
                        return meal;
                    }
                }
            });

            modal.result.then(function(modified_meal) {
                meal.datetime = modified_meal.datetime;
                meal.description = modified_meal.description;
                meal.calories = modified_meal.calories;
                addFormattedDatetime(meal)
            });
        };

        $scope.deleteMeal = function(meal_id) {
            var data = {
                title: 'Delete meal',
                text: 'Are you sure you want to delete meal?'
            };

            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/confirmation-dialog.html',
                controller: 'confirmationDialogCtrl',
                resolve: {
                    data: function () {
                        return data;
                    }
                }
            });

            modal.result.then(function() {
                Meal(custom_headers).delete({username: username, meal_id: meal_id},
                    function (res) {
                        // Removing meal in view.
                        var meal_idx = $scope.meals.findIndex(function(meal) {
                            return meal._id === meal_id;
                        });

                        if (meal_idx !== -1)
                            $scope.meals.splice(meal_idx, 1);

                    },
                    function(err) {
                        Messages.show_error(err);
                    }
                );
            });
        };

        $scope.$watch('meals', function() {
            $scope.calories_today = getCaloriesToday($scope.meals);
        }, true);

        $scope.$watch('current_user_calories_expected_per_day', function() {
            $scope.calories_today_style = getCaloriesRowStyle($scope.current_user_calories_expected_per_day, $scope.calories_today);
        });

        $scope.$watch('calories_today', function() {
            $scope.calories_today_style = getCaloriesRowStyle($scope.current_user_calories_expected_per_day, $scope.calories_today);
        });
        
        $scope.dateFrom = null;
        $scope.dateTo = null;
        $scope.timeFrom = null;
        $scope.timeTo = null;


        $scope.clearTimeFrom = function() {
            $scope.timeFrom = null;
        };

        $scope.clearTimeTo = function() {
            $scope.timeTo = null;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(2000, 1, 1),
            startingDay: 1
        };

        $scope.openDateFrom = function() {
            $scope.dateFromPopup.opened = true;
        };

        $scope.openDateTo = function() {
            $scope.dateToPopup.opened = true;
        };

        $scope.dateFromPopup = {
            opened: false
        };

        $scope.dateToPopup = {
            opened: false
        };
    }
]);
