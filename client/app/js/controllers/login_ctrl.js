'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('LoginCtrl', ['$scope', '$http', '$location', '$cookies', 'UserBasic',
    function($scope, $http, $location, $cookies, UserBasic) {

        $scope.show_login_error_msg = false;

        $scope.username = '';
        $scope.password = '';
        
        $scope.login = function() {
            var login_data = {
                'username': $scope.username,
                'password': $scope.password
            };

            UserBasic().login(login_data,
                function(res) {
                    $cookies.put('auth_token', res.token);
                    $cookies.put('username', res.user.username);
                    $cookies.put('admin', res.user.admin);
                    $cookies.put('calories_expected_per_day', res.user.calories_expected_per_day);
                    
                    if (!res.user.admin) {
                        $location.path('/meals/' + res.user.username);
                    }
                    else {
                        $location.path('/users');
                    }
                },
                function(err) {
                    $scope.show_login_error_msg = true;
                }
            );
        };

        $scope.createNewAccount = function() {
            $location.path('/signup');
        }
    }
]);
