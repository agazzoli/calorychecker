'use strict';

function alertCtrl($scope, $uibModalInstance, data) {
    $scope.data = data;
    $scope.close = function() {
        $uibModalInstance.close($scope.data);
    };
};
