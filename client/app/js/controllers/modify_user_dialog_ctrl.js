'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('modifyUserDialogCtrl',  ['$scope', '$uibModalInstance', '$cookies', 'User', 'username', 'calories_expected_per_day', 'Messages',
    function($scope, $uibModalInstance, $cookies, User, username, calories_expected_per_day, Messages) {

        $scope.username = username;
        $scope.calories_expected_per_day = calories_expected_per_day;

        $scope.saveUser = function () {
            var custom_headers = {"authentication": $cookies.get('auth_token')};

            var update_user_data = {
                'calories_expected_per_day': $scope.calories_expected_per_day
            };

            User(custom_headers).update({username: $scope.username}, update_user_data,
                function(res_update_user) {
                    $uibModalInstance.close(res_update_user.user);
                },
                function(err) {
                    $uibModalInstance.dismiss('cancel');
                    Messages.show_error(err);
                }
            );
        };
        
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);