'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('addNewMealDialogCtrl',  ['$scope', '$uibModalInstance', '$cookies', 'Meal', 'username', 'Messages',
    function($scope, $uibModalInstance, $cookies, Meal, username, Messages) {
    
        $scope.add_dialog = true;

        $scope.date = new Date();
        $scope.time = new Date();
        $scope.description = '';
        $scope.calories = 0;

        $scope.saveMeal = function () {
            var custom_headers = {"authentication": $cookies.get('auth_token')};

            var add_meal_data = {
                'datetime': getDatetime($scope.date, $scope.time),
                'description': $scope.description,
                'calories': $scope.calories
            };

            Meal(custom_headers).add({username: username}, add_meal_data,
                function(res_add_meal) {
                    $uibModalInstance.close(res_add_meal.meal);
                },
                function(err) {
                    $uibModalInstance.dismiss('cancel');
                    Messages.show_error(err);
                }
            );
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        
        $scope.openDate = function() {
            $scope.datePopup.opened = true;
        };

        $scope.datePopup = {
            opened: false
        };
    }
]);
