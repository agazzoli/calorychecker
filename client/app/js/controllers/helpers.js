'use strict';

function getOnlyDateFromDatetime(datetime) {
    datetime.setHours(0);
    datetime.setMinutes(0);
    datetime.setSeconds(0);
    datetime.setMilliseconds(0);
    return datetime;
}

function getOnlyTimeFromDatetime(datetime) {
    datetime.setFullYear(1900);
    datetime.setMonth(0);
    datetime.setDate(1);
    datetime.setSeconds(0);
    datetime.setMilliseconds(0);
    return datetime;
}

function getDatetime(date, time) {
    var datetime = new Date();
    
    datetime.setFullYear(date.getFullYear());
    datetime.setMonth(date.getMonth());
    datetime.setDate(date.getDate());
    datetime.setHours(time.getHours());
    datetime.setMinutes(time.getMinutes());
    datetime.setSeconds(time.getSeconds());
    datetime.setMilliseconds(time.getMilliseconds());
    
    return datetime;
}

function isInRange(meal_date_or_time, date_or_time_from, date_or_time_to) {
    if ((date_or_time_from !== null) && (date_or_time_to !== null)) {
        if ((meal_date_or_time >= date_or_time_from) && (meal_date_or_time <= date_or_time_to))
            return true;
    }
    else if ((date_or_time_from !== null) && (date_or_time_to === null)) {
        if (meal_date_or_time >= date_or_time_from)
            return true;
    }
    else if ((date_or_time_from === null) && (date_or_time_to !== null)) {
        if (meal_date_or_time <= date_or_time_to)
            return true;
    }
    else if ((date_or_time_from === null) && (date_or_time_to === null)) {
        return true;
    }

    return false;
}

function addFormattedDatetime(meal) {
    var meal_date = new Date(meal.datetime);

    meal['formatted_date'] = moment(meal_date).format('MM/DD/YYYY');
    meal['formatted_time'] = moment(meal_date).format('HH:mm');

    return meal;
}

function addFormattedDatetimeToMeals(meals) {
    for (var i = 0; i < meals.length; i++) {
        addFormattedDatetime(meals[i])
    }

    return meals;
}

function getCaloriesToday(meals) {
    if (typeof meals === 'undefined')
        return 0;

    var today = getOnlyDateFromDatetime(new Date());
    var calories_today = 0;

    for (var i = 0; i < meals.length; i++) {
        var meal_date = new Date(meals[i].formatted_date);

        if (isInRange(meal_date, today, today))
            calories_today += meals[i].calories;
    }

    return calories_today;
}

function getCaloriesRowStyle(expected, actual) {
    if (actual <= expected) {
        return {
            'background-color': '#71D8D2'
        }
    }
    else {
        return {
            'background-color': '#EF7893'
        }
    }
}
