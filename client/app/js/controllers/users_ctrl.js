'use strict';

var caloryCheckerControllers = angular.module('caloryChecker.controllers');

caloryCheckerControllers.controller('UsersCtrl', ['$scope', '$cookies', '$location', 'User', '$uibModal', 'Messages',
    function($scope, $cookies, $location, User, $uibModal, Messages) {

        $scope.orderBy = 'username';
    
        var custom_headers = {"authentication": $cookies.get('auth_token')};

        User(custom_headers).get_users(
            function (res) {
                $scope.users = res.users;
            }
        );

        $scope.addNewUser = function() {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/add-user-dialog.html',
                controller: 'addNewUserDialogCtrl'
            });

            modal.result.then(function(added_user) {
                $scope.users.push(added_user);
            });
        };

        $scope.modifyUser = function(username, calories_expected_per_day) {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/modify-user-dialog.html',
                controller: 'modifyUserDialogCtrl',
                resolve: {
                    username: function () {
                      return username;
                    },
                    calories_expected_per_day: function () {
                      return calories_expected_per_day;
                    }
                }
            });

            modal.result.then(function(modified_user) {
                $cookies.put('calories_expected_per_day', modified_user.calories_expected_per_day);

                // Updating user in view.
                var user = $scope.users.find(function(user) {
                    return user.username === modified_user.username;
                });

                if (typeof user !== 'undefined')
                    user.calories_expected_per_day = modified_user.calories_expected_per_day;
            });
        };

        $scope.changeUserPassword = function(username) {
            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/change-user-password-dialog.html',
                controller: 'changeUserPasswordDialogCtrl',
                resolve: {
                    username: function () {
                        return username;
                    },
                    include_current_password: function () {
                        return !(($cookies.get('admin') === 'true') && ($cookies.get('username') !== username));
                    }
                }
            });

            modal.result.then(function() {
                Messages.show_message('Change password:', "User's password was successfully changed", 'success');
            });
        };

        $scope.deleteUser = function(username) {
            var data = {
                title: 'Delete user',
                text: 'Are you sure you want to delete user ' + username + '?'
            };

            var modal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'partials/confirmation-dialog.html',
                controller: 'confirmationDialogCtrl',
                resolve: {
                    data: function () {
                        return data;
                    }
                }
            });

            modal.result.then(function() {
                User(custom_headers).delete({username: username},
                    function (res) {
                        // Removing user in view.
                        var user_idx = $scope.users.findIndex(function(user) {
                            return user.username === username;
                        });

                        if (user_idx !== -1)
                            $scope.users.splice(user_idx, 1);

                    },
                    function(err) {
                        Messages.show_error(err);
                    }
                );
            });
        };
    }
]);


