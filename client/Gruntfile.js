'use strict';

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        karma: {
            unit: {
                configFile: 'test/karma.conf.js',
                singleRun: true
            }
        },
        
        protractor_webdriver: {
            e2e_server: {
                options: {
                    path: 'node_modules/protractor/bin/',
                    command: 'webdriver-manager start --standalone',
                    keepAlive: true
                },
            },
        },
        
        protractor: {
            options: {
                configFile: "test/protractor-conf.js",
                keepAlive: true,
                noColor: false,
                args: {
                    verbose: true
                }

            },
            all: {}
        },
        
        clean: {
            dist: ["dist"],
            css: ["dist/css/animations.css", "dist/css/styles.css"],
            js: ["dist/js"]
        },
        
        copy: {
            all: {
                files: [
                    {src: ['app/bower_components/bootstrap/dist/css/bootstrap.min.css'], dest: 'dist/css/bootstrap.min.css'},
                    {src: ['app/bower_components/angular-material/angular-material.min.css'], dest: 'dist/css/angular-material.min.css'},
                    
                    {expand: true, cwd: 'app/', src: ['css/**'], dest: 'dist/'},
                
                    {src: ['app/bower_components/angular/angular.min.js'], dest: 'dist/lib/angular.min.js'},
                    {src: ['app/bower_components/angular-messages/angular-messages.min.js'], dest: 'dist/lib/angular-messages.min.js'},
                    {src: ['app/bower_components/angular-route/angular-route.min.js'], dest: 'dist/lib/angular-route.min.js'},
                    {src: ['app/bower_components/angular-cookies/angular-cookies.min.js'], dest: 'dist/lib/angular-cookies.min.js'},
                    {src: ['app/bower_components/angular-animate/angular-animate.min.js'], dest: 'dist/lib/angular-animate.min.js'},
                    {src: ['app/bower_components/angular-resource/angular-resource.min.js'], dest: 'dist/lib/angular-resource.min.js'},
                    {src: ['app/bower_components/angular-password/angular-password.min.js'], dest: 'dist/lib/angular-password.min.js'},
                    {src: ['app/bower_components/angular-aria/angular-aria.min.js'], dest: 'dist/lib/angular-aria.min.js'},
                    {src: ['app/bower_components/angular-material/angular-material.min.js'], dest: 'dist/lib/angular-material.min.js'},
                    {src: ['app/bower_components/moment/min/moment.min.js'], dest: 'dist/lib/moment.min.js'},
                    {src: ['app/lib/ui-bootstrap-tpls-2.1.3.min.js'], dest: 'dist/lib/ui-bootstrap-tpls-2.1.3.min.js'},

                    {src: ['app/bower_components/bootstrap/fonts/glyphicons-halflings-regular.ttf'], dest: 'dist/fonts/glyphicons-halflings-regular.ttf'},
                    {src: ['app/bower_components/bootstrap/fonts/glyphicons-halflings-regular.woff'], dest: 'dist/fonts/glyphicons-halflings-regular.woff'},
                    {src: ['app/bower_components/bootstrap/fonts/glyphicons-halflings-regular.woff2'], dest: 'dist/fonts/glyphicons-halflings-regular.woff2'},
                    
                    {expand: true, cwd: 'app/', src: ['images/**'], dest: 'dist/'},
                    {expand: true, cwd: 'app/', src: ['partials/**'], dest: 'dist/'},
                    
                    {src: ['app/favicon.ico'], dest: 'dist/favicon.ico'},
                    
                    {expand: true, cwd: 'app/', src: ['js/**'], dest: 'dist/'}
                ],
            },
        },
        
        cssmin: {
            options: {
                shorthandCompacting: true,
                roundingPrecision: -1
            },
            css: {
                files: {
                    'dist/css/styles.min.css': ['dist/css/animations.css', 'dist/css/styles.css']
                }
            }
        },
        
        uglify: {
            js: {
                files: {
                    'dist/app.min.js': ['dist/js/app.js',
                                        'dist/js/filters/filters.js',
                                        'dist/js/services/config.js',
                                        'dist/js/services/rest_svc.js',
                                        'dist/js/services/messages_svc.js',
                                        'dist/js/controllers/helpers.js',
                                        'dist/js/controllers/config.js',
                                        'dist/js/controllers/alert_ctrl.js',
                                        'dist/js/controllers/confirmation_dialog_ctrl.js',
                                        'dist/js/controllers/toolbar_ctrl.js',
                                        'dist/js/controllers/login_ctrl.js',
                                        'dist/s/controllers/signup_ctrl.js',
                                        'dist/js/controllers/add_new_user_dialog_ctrl.js',
                                        'dist/js/controllers/modify_user_dialog_ctrl.js',
                                        'dist/js/controllers/change_user_password_new_user_dialog_ctrl.js',
                                        'dist/js/controllers/users_ctrl.js',
                                        'dist/js/controllers/add_new_meal_dialog_ctrl.js',
                                        'dist/js/controllers/modify_meal_dialog_ctrl.js',
                                        'dist/js/controllers/meals_ctrl.js'
                    ]
                }
            }
        },
       
        processhtml: {
            index: {
                files: {
                    'dist/index.html': ['app/index.html']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-protractor-webdriver');
    grunt.loadNpmTasks('grunt-protractor-runner');
    
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-processhtml');
            
    grunt.registerTask("test", ["karma"]);
    grunt.registerTask("e2e_test", ["protractor_webdriver:e2e_server", "protractor"]);
    
    grunt.registerTask("clean_build", ["clean:dist"]);
    grunt.registerTask("build", ["clean:dist", "copy:all", "cssmin:css", "clean:css", "uglify:js", "clean:js", "processhtml:index"]);
    
    grunt.registerTask("default", ["protractor_webdriver:e2e_server", "protractor"]);
};
