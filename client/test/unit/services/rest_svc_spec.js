'use strict';

describe('Rest API Services', function () {

    beforeEach(angular.mock.module('caloryChecker.services'));

    var httpBackend;

    beforeEach(inject(function($httpBackend) {
        httpBackend = $httpBackend;
    }));

    describe('Service: UserBasic', function () {

        var UserBasic;

        beforeEach(inject(function(_UserBasic_) {
            UserBasic = _UserBasic_;
        }));

        it('01 - should call login rest api', function() {

            httpBackend.expectPOST('/api/login')
                .respond({ /* not important in this case */ });

            var res = UserBasic().login();

            httpBackend.flush();
        });

        it('02 - should call signup rest api', function() {

            httpBackend.expectPOST('/api/signup')
                .respond({ /* not important in this case */ });

            var res = UserBasic().signup();

            httpBackend.flush();
        });

    });

    describe('Service: User', function () {

        var User;

        beforeEach(inject(function(_User_) {
            User = _User_;
        }));

        it('01 - should call users rest api', function() {

            httpBackend.expectGET('/api/users')
                .respond({ /* not important in this case */ });

            var res = User().get_users();

            httpBackend.flush();
        });

        it('02 - should call user settings rest api', function() {

            httpBackend.expectGET('/api/user')
                .respond({ /* not important in this case */ });

            var res = User().get_user();

            httpBackend.flush();
        });

        it('03 - should call delete user rest api', function() {

            httpBackend.expectPOST('/api/user/add')
                .respond({ /* not important in this case */ });

            var res = User().add();

            httpBackend.flush();
        });

        it('04 - should call change user settings rest api', function() {

            httpBackend.expectPUT('/api/user/update')
                .respond({ /* not important in this case */ });

            var res = User().update();

            httpBackend.flush();
        });

        it('05 - should call change user password rest api', function() {

            httpBackend.expectPUT('/api/user/change-password')
                .respond({ /* not important in this case */ });

            var res = User().change_password();

            httpBackend.flush();
        });

        it('06 - should call delete user rest api', function() {

            httpBackend.expectDELETE('/api/user/delete')
                .respond({ /* not important in this case */ });

            var res = User().delete();

            httpBackend.flush();
        });
    });


    describe('Service: Meal', function () {

        var Meal;

        beforeEach(inject(function(_Meal_) {
            Meal = _Meal_;
        }));


        it('01 - should call meals rest api', function() {

            httpBackend.expectGET('/api/meals')
                .respond({ /* not important in this case */ });

            var res = Meal().get_meals();

            httpBackend.flush();
        });

        it('02 - should call add meal rest api', function() {

            httpBackend.expectPOST('/api/meal/add')
                .respond({ /* not important in this case */ });

            var res = Meal().add();

            httpBackend.flush();
        });

        it('03 - should call update meal rest api', function() {

            httpBackend.expectPUT('/api/meal/update')
                .respond({ /* not important in this case */ });

            var res = Meal().update();

            httpBackend.flush();
        });

        it('04 - should call delete meal rest api', function() {

            httpBackend.expectDELETE('/api/meal/delete')
                .respond({ /* not important in this case */ });

            var res = Meal().delete();

            httpBackend.flush();
        });
    });
});
