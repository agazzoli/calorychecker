'use strict';

describe('Service: Messages', function () {

    // General dependencies.
    var Messages;

    // Mocks.
    var mockUibModal;

    beforeEach(angular.mock.module('caloryChecker.services'));

    beforeEach(function() {
        mockUibModal = {
            open: jasmine.createSpy()
        };

        module(function($provide) {
            $provide.value('$uibModal', mockUibModal);
        });

        inject(function(_Messages_) {
            Messages = _Messages_;
        });


    });

    it('01 - should open a dialog', function() {
        Messages.open_dialog({});
        expect(mockUibModal.open).toHaveBeenCalled();
    });
	
	it('02 - should open a dialog when show an error', function() {
        Messages.show_error({data: {msg: 'Message'}});
        expect(mockUibModal.open).toHaveBeenCalled();
    });

    it('03 - should open a dialog when show a message', function() {
        Messages.show_message('Title', 'Message', 'mode');
        expect(mockUibModal.open).toHaveBeenCalled();
    });

});