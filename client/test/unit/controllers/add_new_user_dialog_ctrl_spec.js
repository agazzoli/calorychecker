'use strict';

describe('Controller: addNewUserDialogCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $cookies;
    var deferred;

    // Mocks.
    var mockUser;
    var mockModalInstance;
    var mockMessages;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$cookies_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $cookies = _$cookies_;
    }));

    beforeEach(function() {
        deferred = $q.defer();

        mockUser = function() {
            return {
                add: function(login_data, success_fx, error_fx) {
                    deferred.promise.then(success_fx, error_fx);
                    return deferred;
                }
            };
        };

        mockModalInstance = {
            close: jasmine.createSpy(),
            dismiss: jasmine.createSpy()
        };

        mockMessages = {
            show_error: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            User: mockUser,
            $uibModalInstance: mockModalInstance,
            Messages: mockMessages
        };

        $controller('addNewUserDialogCtrl', locals);
    });

    it('01 - should close modal dialog if dialog is cancelled', function() {
        $scope.cancel();
        expect(mockModalInstance.dismiss).toHaveBeenCalled();
    });

    it('02 - should close modal dialog and show an error when a new user can not be created', function() {
        $cookies.put('auth_token', 'my_token');

        $scope.username = 'andres';
        $scope.password = 'pass';
        $scope.password2 = 'pass';
        $scope.admin = false;
        $scope.calories_expected_per_day = 1000;

        $scope.saveUser();

        deferred.reject({
            "success": false,
            "msg": 'Username already used!'
        });

        $scope.$apply();

        expect(mockModalInstance.dismiss).toHaveBeenCalled();
        expect(mockMessages.show_error).toHaveBeenCalled();
    });

    it('03 - should close modal dialog when a new user is created', function () {
        $cookies.put('auth_token', 'my_token');

        $scope.username = 'andres';
        $scope.password = 'pass';
        $scope.password2 = 'pass';
        $scope.admin = false;
        $scope.calories_expected_per_day = 1000;

        $scope.saveUser();

        deferred.resolve({
            "success": false,
            msg: 'User successfully saved!',
            user: {
                username: 'andres',
                admin: true,
                calories_expected_per_day: 1000
            }
        });

        $scope.$apply();

        expect(mockModalInstance.close).toHaveBeenCalled();
    });
});

