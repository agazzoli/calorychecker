'use strict';

describe('Controller: modifyUserDialogCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $cookies;
    var deferred;

    // Mocks.
    var mockUser;
    var mockModalInstance;
    var mockMessages;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$cookies_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $cookies = _$cookies_;
    }));

    beforeEach(function() {
        deferred = $q.defer();

        mockUser = function() {
            return {
                update: function(params, update_data, success_fx, error_fx) {
                    deferred.promise.then(success_fx, error_fx);
                    return deferred;
                }
            };
        };

        mockModalInstance = {
            close: jasmine.createSpy(),
            dismiss: jasmine.createSpy()
        };

        mockMessages = {
            show_error: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            User: mockUser,
            $uibModalInstance: mockModalInstance,
            Messages: mockMessages,
            username: 'andres',
            calories_expected_per_day: 1000
        };

        $controller('modifyUserDialogCtrl', locals);
    });

    it('01 - should close modal dialog if dialog is cancelled', function() {
        $scope.cancel();
        expect(mockModalInstance.dismiss).toHaveBeenCalled();
    });

    it('02 - should close modal dialog and show an error when a user can not be modified', function() {
        $cookies.put('auth_token', 'my_token');

        $scope.username = 'andres';
        $scope.calories_expected_per_day = 2000;

        $scope.saveUser();

        deferred.reject({
            success: false,
            msg: 'User not found!'}
        );

        $scope.$apply();

        expect(mockModalInstance.dismiss).toHaveBeenCalled();
        expect(mockMessages.show_error).toHaveBeenCalled();
    });

    it('03 - should close modal dialog when a user is modified', function () {
        $cookies.put('auth_token', 'my_token');

        $scope.username = 'andres';
        $scope.calories_expected_per_day = 2000;

        $scope.saveUser();

        deferred.resolve({
            success: false,
            msg: 'User successfully updated!',
            user: {
                username: 'andres',
                admin: true,
                calories_expected_per_day: 2000
            }
        });

        $scope.$apply();

        expect(mockModalInstance.close).toHaveBeenCalled();
    });
});

