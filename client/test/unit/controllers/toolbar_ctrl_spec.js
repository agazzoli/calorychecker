'use strict';

describe('Controller: ToolbarCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var basic_scope;
    var $scope;
    var $location;
    var $cookies;
    var $uibModal;
    var $routeParams;
    var deferredGetUsers;
    var deferredDelete;
    var deferredUibModal;

    // Mocks.
    var mockUibModal;
    var mockMessages;
    var mockLocation;
    
    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$location_, _$cookies_, _$uibModal_) {
        $controller = _$controller_;
        $q = _$q_;
        basic_scope = _$rootScope_.$new();
        basic_scope.users = [
            {
                "_id": "574e51a659031468149ad6e3",
                "username": "admin",
                "admin": true,
                "calories_expected_per_day": 2000
            }
        ];
        $scope = basic_scope.$new().$new();
        $location = _$location_;
        $cookies = _$cookies_;
        $uibModal = _$uibModal_;
    }));
    
    beforeEach(function() {
        deferredUibModal = $q.defer();

        mockUibModal = {
            open: function(config, success_fx, error_fx) {
                deferredUibModal.promise.then(success_fx, error_fx);
                return { result: deferredUibModal.promise };
            }
        };

        mockMessages = {
            show_message: jasmine.createSpy()
        };
        
        mockLocation = {
            path: jasmine.createSpy()
        };
    });

    it("01 - should change user's password", function () {
        var locals = {
            $scope: $scope, 
            $uibModal: mockUibModal, 
            $location: {}, 
            $routeParams: {},
            $cookies: $cookies, 
            Messages: mockMessages
        };

        $controller('ToolbarCtrl', locals);
        
        $cookies.put('username', 'admin');
        $cookies.put('calories_expected_per_day', 2000);
    
        $scope.changeUserPassword();
               
        deferredUibModal.resolve();
        $scope.$apply();
             
        expect(mockMessages.show_message).toHaveBeenCalled(); 
    });
  
    it("02 - should change user's settings in users page", function () {
        var locals = {
            $scope: $scope, 
            $uibModal: mockUibModal, 
            $location: {
                path: function() { return '/users' }
            }, 
            $routeParams: { 
                username: 'admin'
            },
            $cookies: $cookies, 
            Messages: mockMessages
        };

        $controller('ToolbarCtrl', locals);
        
        $cookies.put('username', 'admin');
        $cookies.put('calories_expected_per_day', 2000);
    
        $scope.changeUserSettings();
        
        var modified_user = {
            "_id": "574e51a659031468149ad6e3",
            "username": "admin",
            "admin": true,
            "calories_expected_per_day": 3500
        };
        
        deferredUibModal.resolve(modified_user);
        $scope.$apply();
             
        expect($cookies.get('calories_expected_per_day')).toBe('3500'); 
        
        var user = $scope.$parent.$parent.users.find(function(user) {
            return user.username === modified_user.username;
        });
        
        expect(user.calories_expected_per_day).toBe(3500);
    });
  
    it("03 - should change user's settings in meals page", function () {
        var locals = {
            $scope: $scope, 
            $uibModal: mockUibModal, 
            $location: {
                path: function() { return '/meals' }
            }, 
            $routeParams: { 
                username: 'admin'
            },
            $cookies: $cookies, 
            Messages: mockMessages
        };

        $controller('ToolbarCtrl', locals);
        
        $cookies.put('username', 'admin');
        $cookies.put('calories_expected_per_day', 2000);
    
        $scope.changeUserSettings();
        
        var modified_user = {
            "_id": "574e51a659031468149ad6e3",
            "username": "admin",
            "admin": true,
            "calories_expected_per_day": 3500
        };
        
        deferredUibModal.resolve(modified_user);
        $scope.$apply();
             
        expect($cookies.get('calories_expected_per_day')).toBe('3500'); 
        expect($scope.$parent.$parent.current_user_calories_expected_per_day).toBe(3500);
    });
    
    it("04 - should log out an user", function () {
        var locals = {
            $scope: $scope, 
            $uibModal: mockUibModal, 
            $location: mockLocation,
            $routeParams: { 
                username: 'admin'
            },
            $cookies: $cookies, 
            Messages: mockMessages
        };
    
        $controller('ToolbarCtrl', locals);
        
        $cookies.put('auth_token', 'token');
        $cookies.put('username', 'admin');
        $cookies.put('admin', true);
        $cookies.put('calories_expected_per_day', 3000);
    
        $scope.logOut();
        
        expect(mockLocation.path).toHaveBeenCalled(); 
        
        expect($cookies.get('auth_token')).toBeUndefined();
        expect($cookies.get('username')).toBeUndefined();
        expect($cookies.get('admin')).toBeUndefined();
        expect($cookies.get('calories_expected_per_day')).toBeUndefined();
    });
});

