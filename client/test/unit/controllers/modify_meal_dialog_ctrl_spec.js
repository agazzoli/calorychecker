'use strict';

describe('Controller: modifyMealDialogCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $cookies;
    var deferred;

    // Mocks.
    var mockMeal;
    var mockModalInstance;
    var mockMessages;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$cookies_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $cookies = _$cookies_;
    }));

    beforeEach(function() {
        deferred = $q.defer();

        mockMeal = function() {
            return {
                update: function(params, modify_meal_data, success_fx, error_fx) {
                    deferred.promise.then(success_fx, error_fx);
                    return deferred;
                }
            };
        };

        mockModalInstance = {
            close: jasmine.createSpy(),
            dismiss: jasmine.createSpy()
        };

        mockMessages = {
            show_error: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            Meal: mockMeal,
            $uibModalInstance: mockModalInstance,
            Messages: mockMessages,
            username: 'andres',
            meal: {
                _id: 'fake_id',
                datetime: new Date(),
                description: 'pasta',
                calories: 2000
            }
        };

        $controller('modifyMealDialogCtrl', locals);
    });

    it('01 - should close modal dialog if dialog is cancelled', function() {
        $scope.cancel();
        expect(mockModalInstance.dismiss).toHaveBeenCalled();
    });

    it('02 - should close modal dialog and show an error when a meal can not be modified', function() {
        $cookies.put('auth_token', 'my_token');

        $scope.date = new Date();
        $scope.time = new Date();
        $scope.description = 'churrasco';
        $scope.calories = 1000;

        $scope.saveMeal();

        deferred.reject({
            success: false,
            msg: 'Meal not found!'}
        );

        $scope.$apply();

        expect(mockModalInstance.dismiss).toHaveBeenCalled();
        expect(mockMessages.show_error).toHaveBeenCalled();
    });

    it('03 - should close modal dialog when a meal is modified', function () {
        $cookies.put('auth_token', 'my_token');

        $scope.date = new Date();
        $scope.time = new Date();
        $scope.description = 'churrasco';
        $scope.calories = 1000;

        $scope.saveMeal();
        
        deferred.resolve({
            success: true,
            msg: 'Meal successfully updated!',
            meal: {
                username: 'andres',
                datetime: new Date(),
                description: 'pasta',
                calories: 2000
            }
        });

        $scope.$apply();

        expect(mockModalInstance.close).toHaveBeenCalled();
    });
});

