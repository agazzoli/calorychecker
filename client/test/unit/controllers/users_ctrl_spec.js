'use strict';

describe('Controller: UsersCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $http;
    var $location;
    var $cookies;
    var $uibModal;
    var deferredGetUsers;
    var deferredDelete;
    var deferredUibModal;

    // Mocks.
    var mockUser;
    var mockUibModal;
    var mockMessages;
    
    beforeEach(angular.mock.module('caloryChecker.controllers'));
    
    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$http_, _$location_, _$cookies_, _$uibModal_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $http = _$http_;
        $location = _$location_;
        $cookies = _$cookies_;
        $uibModal = _$uibModal_;
    }));
    
    beforeEach(function() {
        deferredGetUsers = $q.defer();
        deferredDelete = $q.defer();
        deferredUibModal = $q.defer();

        mockUser = function() {
            return {
                get_users: function(success_fx, error_fx) {
                    deferredGetUsers.promise.then(success_fx, error_fx);
                    return deferredGetUsers;
                },
                delete: function(data, success_fx, error_fx) {
                    deferredDelete.promise.then(success_fx, error_fx);
                    return deferredDelete;
                }
            };
        };
        
        mockUibModal = {
            open: function(config, success_fx, error_fx) {
                deferredUibModal.promise.then(success_fx, error_fx);
                return { result: deferredUibModal.promise };
            }
        };

        mockMessages = {
            show_error: jasmine.createSpy(),
            show_message: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope, 
            User: mockUser, 
            $http: $http, 
            $location: $location, 
            $cookies: $cookies, 
            $uibModal: mockUibModal, 
            Messages: mockMessages
        };

        $controller('UsersCtrl', locals);
    });

    // Get current list of users.    
    beforeEach(function() {
        var users =  [
            {
                "_id": "574bacb3b2aae97c23fb263b",
                "username": "admin",
                "admin": true,
                "calories_expected_per_day": 789
            },
            {
                "_id": "574e51a659031468149ad6e3",
                "username": "andres",
                "admin": false,
                "calories_expected_per_day": 100
            }
        ];

        var res = {
            "success": true,
            "msg": "Users successfully retrieved!",
            "users": users
        };

        deferredGetUsers.resolve(res);
        $scope.$apply();
    });

    it('01 - should show a list of users', function () {
        expect($scope.users.length).toBe(2);
    });

    it('02 - should add a new user', function () {
        $scope.addNewUser();
        
        var new_user = {
            "_id": "574e51a659031468149ad6e3",
            "username": "andres",
            "admin": false,
            "calories_expected_per_day": 100
        };
        
        deferredUibModal.resolve(new_user);
        $scope.$apply();
             
        expect($scope.users.length).toBe(3);
    });
    
    it("03 - should modify a user", function () {
        $scope.modifyUser('andres', 500);
        
        var modified_user = {
            "_id": "574e51a659031468149ad6e3",
            "username": "andres",
            "admin": false,
            "calories_expected_per_day": 500
        };
        
        deferredUibModal.resolve(modified_user);
        $scope.$apply();
             
        expect($cookies.get('calories_expected_per_day')).toBe('500'); 
        
         var user = $scope.users.find(function(user) {
            return user.username === modified_user.username;
        });
        
        expect(user.calories_expected_per_day).toBe(500);
    });
    
    it("04 - should change a user's password", function () {
        $scope.changeUserPassword('andres');
        
        deferredUibModal.resolve();
        $scope.$apply();
             
        expect(mockMessages.show_message).toHaveBeenCalled(); 
    });
    
    it("05 - should delete a user", function () {
        var username = 'andres';
        $scope.deleteUser(username);

        deferredDelete.resolve();
        deferredUibModal.resolve();
        $scope.$apply();
        
         var user_idx = $scope.users.findIndex(function(user) {
            return user.username === username;
        });
        
        expect(user_idx).toBe(-1);
    });
    
    it("06 - should show an error when user can not be deleted", function () {
        var username = 'andres';
        $scope.deleteUser(username);

        deferredDelete.reject();
        deferredUibModal.resolve();
        $scope.$apply();
        
         var user_idx = $scope.users.findIndex(function(user) {
            return user.username === username;
        });
        
        expect(user_idx).not.toBe(-1);
        expect(mockMessages.show_error).toHaveBeenCalled(); 
    });
});

