'use strict';

describe('Controller: MealsCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $http;
    var $location;
    var $cookies;
    var $uibModal;
    var deferredGetMeals;
    var deferredGetUser;
    var deferredDelete;
    var deferredUibModal;

    // Mocks.
    var mockMeal;
    var mockUser;
    var mockUibModal;
    var mockMessages;
    
    beforeEach(angular.mock.module('caloryChecker.controllers'));
    
    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$http_, _$location_, _$cookies_, _$uibModal_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $http = _$http_;
        $location = _$location_;
        $cookies = _$cookies_;
        $uibModal = _$uibModal_;
    }));
    
    beforeEach(function() {
        deferredGetMeals = $q.defer();
        deferredGetUser = $q.defer();
        deferredDelete = $q.defer();
        deferredUibModal = $q.defer();
        
        mockMeal = function() {
            return {
                get_meals: function(data, success_fx, error_fx) {
                    deferredGetMeals.promise.then(success_fx, error_fx);
                    return deferredGetMeals;
                },
                delete: function(data, success_fx, error_fx) {
                    deferredDelete.promise.then(success_fx, error_fx);
                    return deferredDelete;
                }
            };
        };

        mockUser = function() {
            return {
                get_user: function(data, success_fx, error_fx) {
                    deferredGetUser.promise.then(success_fx, error_fx);
                    return deferredGetUser;
                }
            };
        };
        
        mockUibModal = {
            open: function(config, success_fx, error_fx) {
                deferredUibModal.promise.then(success_fx, error_fx);
                return { result: deferredUibModal.promise };
            }
        };

        mockMessages = {
            show_error: jasmine.createSpy(),
            show_message: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope, 
            Meal: mockMeal,
            User: mockUser,
            $routeParams: {
                username: 'admin'
            },
            $http: $http, 
            $location: $location, 
            $cookies: $cookies, 
            $uibModal: mockUibModal,
            Messages: mockMessages
        };

        $controller('MealsCtrl', locals);
    });

    // Get user from meal list.
    beforeEach(function() {
        var user = {
            "_id": "574bacb3b2aae97c23fb263b",
            "username": "admin",
            "admin": true,
            "calories_expected_per_day": 2000
        };

        var res = {
            "success": true,
            "msg": "Users successfully retrieved!",
            "user": user
        };

        deferredGetUser.resolve(res);
        $scope.$apply();
    });
    
    // Get user' meals.
    beforeEach(function() {
        var meals = [
            {
                "_id": "664bacb3b2bbe97c23fb263b",
                "datetime": new Date(),
                "description": 'pasta',
                "calories": 1000
            }
        ];

        var res = {
            'success': true,
            'msg': "user's meals were successfully retrieved!",
            'meals': meals
        };

        deferredGetMeals.resolve(res);
        $scope.$apply();
    });

    it('01 - should show a list of meals', function () {
        expect($scope.meals.length).toBe(1);
    
        expect($scope.calories_today).toBe(1000);
        expect($scope.meals[0].hasOwnProperty('formatted_date')).toBe(true);
        expect($scope.meals[0].hasOwnProperty('formatted_time')).toBe(true);
    
        expect($scope.current_user_calories_expected_per_day).toBe(2000);
        expect($scope.calories_today_style).toEqual({
            'background-color': '#71D8D2'
        });
    });

    it('02 - should add a new meal', function () {
        $scope.addNewMeal();
        
        var new_meal = {
            "_id": "884bacb3b2bbe97c23fb263b",
            "datetime": new Date(),
            "description": 'churrasco',
            "calories": 1500
        };
        
        deferredUibModal.resolve(new_meal);
        $scope.$apply();
             
        expect($scope.meals.length).toBe(2);
    });

    it("03 - should modify a meal", function () {
        var meal = {
            "_id": "664bacb3b2bbe97c23fb263b",
            "datetime": new Date(),
            "description": 'pasta',
            "calories": 1000
        };
        
        $scope.modifyMeal(meal);
        
        var modified_meal = {
            "_id": "664bacb3b2bbe97c23fb263b",
            "datetime": new Date(),
            "description": 'salad',
            "calories": 500
        };
       
        deferredUibModal.resolve(modified_meal);
        $scope.$apply();
        
        expect(meal.description).toBe('salad');        
        expect(meal.calories).toBe(500);
    });

    it("04 - should delete a meal", function () {
        var username = 'andres';
        var meal_id = '664bacb3b2bbe97c23fb263b';
        $scope.deleteMeal({username: username, meal_id: meal_id});

        deferredDelete.resolve();
        deferredUibModal.resolve();
        $scope.$apply();
        
        var meal_idx = $scope.meals.findIndex(function(meal) {
            return meal._id === meal_id;
        });
        
        expect(meal_idx).toBe(0);
    });
    
    it("05 - should show an error when meal can not be deleted", function () {
        var username = 'andres';
        var meal_id = '664bacb3b2bbe97c23fb263b';
        $scope.deleteMeal({username: username, meal_id: meal_id});

        deferredDelete.reject();
        deferredUibModal.resolve();
        $scope.$apply();
        
        var meal_idx = $scope.meals.findIndex(function(meal) {
            return meal._id === meal_id;
        });
        
        expect(meal_idx).not.toBe(-1);
        expect(mockMessages.show_error).toHaveBeenCalled(); 
    });
});

