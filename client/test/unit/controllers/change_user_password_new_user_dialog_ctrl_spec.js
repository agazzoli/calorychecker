'use strict';

describe('Controller: changeUserPasswordDialogCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $cookies;
    var deferred;

    // Mocks.
    var mockUser;
    var mockModalInstance;
    var mockMessages;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$cookies_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $cookies = _$cookies_;
    }));

    beforeEach(function() {
        deferred = $q.defer();

        mockUser = function() {
            return {
                change_password: function(params, change_password_data, success_fx, error_fx) {
                    deferred.promise.then(success_fx, error_fx);
                    return deferred;
                }
            };
        };

        mockModalInstance = {
            close: jasmine.createSpy(),
            dismiss: jasmine.createSpy()
        };

        mockMessages = {
            show_error: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            User: mockUser,
            $uibModalInstance: mockModalInstance,
            Messages: mockMessages,
            username: 'andres',
            include_current_password: false
        };

        $controller('changeUserPasswordDialogCtrl', locals);
    });

    it('01 - should close modal dialog if dialog is cancelled', function() {
        $scope.cancel();
        expect(mockModalInstance.dismiss).toHaveBeenCalled();
    });

    it("02 - should close modal dialog and show an error when a user's password can not be changed", function() {
        $cookies.put('auth_token', 'my_token');

        $scope.username = 'andres';
        $scope.current_password = 'wrongpass';
        $scope.new_password = 'newpass';
        $scope.new_password2 = 'newpass';

        $scope.saveUser();

        deferred.reject({
            success: false,
            msg: 'Wrong password!'}
        );

        $scope.$apply();

        expect(mockModalInstance.dismiss).toHaveBeenCalled();
        expect(mockMessages.show_error).toHaveBeenCalled();
    });

    it("03 - should close modal dialog when a user's password is changed", function () {
        $cookies.put('auth_token', 'my_token');

        $scope.username = 'andres';
        $scope.current_password = 'pass';
        $scope.new_password = 'newpass';
        $scope.new_password2 = 'newpass';

        $scope.saveUser();

        deferred.resolve({
            success: false,
            msg:"User's password successfully changed!"
        });

        $scope.$apply();

        expect(mockModalInstance.close).toHaveBeenCalled();
    });
});

