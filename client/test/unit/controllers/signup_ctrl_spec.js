'use strict';

describe('Controller: SignupCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $location;
    var $cookies;
    var deferred;

    // Mocks
    var mockUserBasic;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$location_, _$cookies_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $cookies = _$cookies_;
    }));

    beforeEach(function() {
        deferred = $q.defer();

        mockUserBasic = function() {
            return {
                signup: function(signup_data, success_fx, error_fx) {
                    deferred.promise.then(success_fx, error_fx);
                    return deferred;
                }
            };
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            UserBasic: mockUserBasic
        };

        $controller('SignupCtrl', locals);
    });

    it('01 - should show an error message if sign up fails', function() {
        $scope.username = 'andres';
        $scope.password = 'password';
        $scope.calories_expected_per_day = 2000;

        $scope.signup();

        deferred.reject({
            data: {
                "success": false,
                "msg": "Username already used!"
            }
        });

        $scope.$apply();

        expect($scope.show_signup_error_msg).toBe(true);
        expect($scope.error_msg).toBe("Username already used!");
    });

    it('02 - should show a new account created message when sign up a new user', function () {
        $scope.username = 'andres';
        $scope.password = 'password';
        $scope.calories_expected_per_day = 2000;

        $scope.signup();

        deferred.resolve({});

        $scope.$apply();

        expect($scope.show_new_account_created_msg).toBe(true);
    });

    it('03 - should redirect to / when log in button is pressed', function () {
        $scope.goBackToLogIn();
        expect($location.path()).toBe('/');
    });
});

