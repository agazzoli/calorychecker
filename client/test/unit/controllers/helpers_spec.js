'use strict';

describe('Helpers', function () {

    it('01 - should return the same date but time in 0', function() {
        var d = new Date();
        var datetime = getOnlyDateFromDatetime(d);
       
        expect(datetime.getFullYear()).toBe(d.getFullYear());
        expect(datetime.getMonth()).toBe(d.getMonth());
        expect(datetime.getDate()).toBe(d.getDate());
        
        expect(datetime.getHours()).toBe(0);
        expect(datetime.getMinutes()).toBe(0);
        expect(datetime.getSeconds()).toBe(0);
        expect(datetime.getMilliseconds()).toBe(0);
    });

    it('02 - should return the same time but 1900-01-01 as date', function() {
        var d = new Date();
        var datetime = getOnlyTimeFromDatetime(d);
       
        expect(datetime.getFullYear()).toBe(1900);
        expect(datetime.getMonth()).toBe(0);
        expect(datetime.getDate()).toBe(1);
        
        expect(datetime.getHours()).toBe(d.getHours());
        expect(datetime.getMinutes()).toBe(d.getMinutes());
        expect(datetime.getSeconds()).toBe(0);
        expect(datetime.getMilliseconds()).toBe(0);
    });
    
    it('03 - should return a datetime using the date and the time from different datetimes', function() {
        var d = new Date();
        
        var date_part = getOnlyDateFromDatetime(d);
        var time_part = getOnlyTimeFromDatetime(d);
        
        var datetime = getDatetime(date_part, time_part);
       
        expect(datetime.getFullYear()).toBe(1900);
        expect(datetime.getMonth()).toBe(0);
        expect(datetime.getDate()).toBe(1);
        
        expect(datetime.getHours()).toBe(0);
        expect(datetime.getMinutes()).toBe(0);
        expect(datetime.getSeconds()).toBe(0);
        expect(datetime.getMilliseconds()).toBe(0);
    });
    
    //-----------------------------------------------------------------------------
    it('04 - should return true if date is between date from and date to', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-10-10T10:10:05'));
        var date_from = getOnlyDateFromDatetime(new Date('2016-09-10T10:10:05'));
        var date_to = getOnlyDateFromDatetime(new Date('2016-11-10T10:10:05'));
        
        expect(isInRange(date, date_from, date_to)).toBe(true);
    });
    
    it('05 - should return false if date is between date from and date to', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-01-10T10:10:05'));
        var date_from = getOnlyDateFromDatetime(new Date('2016-09-10T10:10:05'));
        var date_to = getOnlyDateFromDatetime(new Date('2016-11-10T10:10:05'));
        
        expect(isInRange(date, date_from, date_to)).toBe(false);
    });
    
    it('06 - should return true if date is post date from', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-10-10T10:10:05'));
        var date_from = getOnlyDateFromDatetime(new Date('2016-09-10T10:10:05'));
        var date_to = null;
        
        expect(isInRange(date, date_from, date_to)).toBe(true);
    });
    
    it('07 - should return false if date is not post date from', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-01-10T10:10:05'));
        var date_from = getOnlyDateFromDatetime(new Date('2016-09-10T10:10:05'));
        var date_to = null;
        
        expect(isInRange(date, date_from, date_to)).toBe(false);
    });

    it('08 - should return true if date is previous date to', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-10-10T10:10:05'));
        var date_from = null;
        var date_to = getOnlyDateFromDatetime(new Date('2016-11-10T10:10:05'));
        
        expect(isInRange(date, date_from, date_to)).toBe(true);
    });
    
    it('09 - should return false if date is not previous date to', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-12-10T10:10:05'));
        var date_from = null;
        var date_to = getOnlyDateFromDatetime(new Date('2016-11-10T10:10:05'));
        
        expect(isInRange(date, date_from, date_to)).toBe(false);
    });
    
    it('10 - should return true if date from and date to are null', function() {
        var date = getOnlyDateFromDatetime(new Date('2016-12-10T10:10:05'));
        var date_from = null;
        var date_to = null;
        
        expect(isInRange(date, date_from, date_to)).toBe(true);
    });
    
    //-----------------------------------------------------------------------------
    it('11 - should return true if time is between time from and time to', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T10:10:05'));
        var time_from = getOnlyTimeFromDatetime(new Date('2016-09-10T08:01:05'));
        var time_to = getOnlyTimeFromDatetime(new Date('2016-11-10T12:15:05'));
        
        expect(isInRange(time, time_from, time_to)).toBe(true);
    });
    
    it('12 - should return false if time is not between time from and time to', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T15:10:05'));
        var time_from = getOnlyTimeFromDatetime(new Date('2016-09-10T08:01:05'));
        var time_to = getOnlyTimeFromDatetime(new Date('2016-11-10T12:15:05'));
        
        expect(isInRange(time, time_from, time_to)).toBe(false);
    });
        
    it('13 - should return true if time is post time from', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T10:10:05'));
        var time_from = getOnlyTimeFromDatetime(new Date('2016-09-10T08:01:05'));
        var time_to = null;
        
        expect(isInRange(time, time_from, time_to)).toBe(true);
    });
    
    it('14 - should return false if time is not post time from', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T07:10:05'));
        var time_from = getOnlyTimeFromDatetime(new Date('2016-09-10T08:01:05'));
        var time_to = null;
        
        expect(isInRange(time, time_from, time_to)).toBe(false);
    });
    
    it('15 - should return true if time is previous time to', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T10:10:05'));
        var time_from = null;
        var time_to = getOnlyTimeFromDatetime(new Date('2016-11-10T12:15:05'));
        
        expect(isInRange(time, time_from, time_to)).toBe(true);
    });
    
    it('16 - should return false if time is not previous time to', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T15:10:05'));
        var time_from = null;
        var time_to = getOnlyTimeFromDatetime(new Date('2016-11-10T12:15:05'));
        
        expect(isInRange(time, time_from, time_to)).toBe(false);
    });
    
    it('17 - should return true if time from and time to are null', function() {
        var time = getOnlyTimeFromDatetime(new Date('2016-10-10T15:10:05'));
        var time_from = null;
        var time_to = null;
        
        expect(isInRange(time, time_from, time_to)).toBe(true);
    });
    
    //-----------------------------------------------------------------------------
    it('18 - should return a green color if expected calories are greater than actual calories', function() {
        expect(getCaloriesRowStyle(100, 50)).toEqual({
            'background-color': '#71D8D2'
        });
    });
    
    it('19 - should return a green color if expected calories are equal than actual calories', function() {
        expect(getCaloriesRowStyle(100, 100)).toEqual({
            'background-color': '#71D8D2'
        });
    });
    
    it('20 - should return a green color if expected calories are less than actual calories', function() {
        expect(getCaloriesRowStyle(100, 150)).toEqual({
            'background-color': '#EF7893'
        });
    });
});

