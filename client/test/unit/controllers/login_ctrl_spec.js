'use strict';

describe('Controller: LoginCtrl', function () {

    // General dependencies.
    var $controller;
    var $q;
    var $scope;
    var $location;
    var $cookies;
    var deferred;

    // Mocks.
    var mockUserBasic;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$q_, _$rootScope_, _$location_, _$cookies_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $cookies = _$cookies_;
    }));

    beforeEach(function() {
        deferred = $q.defer();

        mockUserBasic = function() {
            return {
                login: function(login_data, success_fx, error_fx) {
                    deferred.promise.then(success_fx, error_fx);
                    return deferred;
                }
            };
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            UserBasic: mockUserBasic
        };

        $controller('LoginCtrl', locals);
    });
    
    it('01 - should show an error message if login fails', function() {
        $scope.username = 'admin';
        $scope.password = 'wrong password';

        $scope.login();

        deferred.reject({
            "success": false,
            "msg": "User or password wrong!"
        });

        $scope.$apply();

        expect($scope.show_login_error_msg).toBe(true);
    });

    it('02 - should set several cookies and redirect to /users when log in an user admin', function () {
        $scope.username = 'admin';
        $scope.password = 'admin';

        $scope.login();

        deferred.resolve({token: 'token', user: { username: 'andres', admin: true, calories_expected_per_day: 2000 } });

        $scope.$apply();

        expect($location.path()).toBe('/users');
        expect($cookies.get('auth_token')).toBe('token');
        expect($cookies.get('username')).toBe('andres');
        expect($cookies.get('admin')).toBe('true');
        expect($cookies.get('calories_expected_per_day')).toBe('2000');
    });

    it('03 - should set several cookies and redirect to /meals when log in a not admin user', function () {
        $scope.username = 'andres';
        $scope.password = 'password';

        $scope.login();

        deferred.resolve({token: 'token', user: { username: 'andres', admin: false, calories_expected_per_day: 2000 } });

        $scope.$apply();

        expect($location.path()).toBe('/meals/andres');
        expect($cookies.get('auth_token')).toBe('token');
        expect($cookies.get('username')).toBe('andres');
        expect($cookies.get('admin')).toBe('false');
        expect($cookies.get('calories_expected_per_day')).toBe('2000');
    });
    
    it('04 - should redirect to /signup when create new account button is pressed', function () {
        $scope.createNewAccount();
        expect($location.path()).toBe('/signup');
    });
});

