'use strict';

describe('Controller: confirmationDialogCtrl', function () {

    // General dependencies.
    var $controller;
    var $scope;

    // Mocks.
    var mockModalInstance;

    beforeEach(angular.mock.module('caloryChecker.controllers'));

    beforeEach(angular.mock.inject(function(_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
    }));

    beforeEach(function() {
        mockModalInstance = {
            close: jasmine.createSpy(),
            dismiss: jasmine.createSpy()
        };
    });

    beforeEach(function() {
        var locals = {
            $scope: $scope,
            $uibModalInstance: mockModalInstance,
            data: {
                title: 'title',
                text: 'text'
            }
        };

        $controller('confirmationDialogCtrl', locals);
    });

    it('01 - should close modal dialog if cancel button is pressed', function() {
        $scope.cancel();
        expect(mockModalInstance.dismiss).toHaveBeenCalled();
    });
    
    it('02 - should close modal dialog if ok button is pressed', function() {
        $scope.ok();
        expect(mockModalInstance.close).toHaveBeenCalled();
    });
});

