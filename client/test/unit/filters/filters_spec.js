'use strict';

describe('datetimefilter', function() {
  
    var datetimefilter;

    beforeEach(angular.mock.module('caloryChecker.filters'));
  
    beforeEach(inject(function(_datetimefilterFilter_) {
        datetimefilter = _datetimefilterFilter_;
    }));

    //---------------------------------------------------------------------------
    // Empty case.
    it('01 - An empty list should return an empty list', function() {
        var items = [];
        var dateFrom = new Date(); // Time will be ignored.
        var dateTo = new Date(); // Time will be ignored.
        var timeFrom = new Date(); // Date will be ignored.
        var timeTo = new Date(); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(0);
    });
    
    //---------------------------------------------------------------------------
    // Date cases.
    it('02 - A list with 1 item should return a list with 1 item if it is between the date from and date to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2016-12-10T00:00:00')
            }       
        ];
        
        var dateFrom = new Date('2015-10-12T10:15:12'); // Time will be ignored.
        var dateTo = new Date('2016-12-12T10:15:12'); // Time will be ignored.
        var timeFrom = null;
        var timeTo = null;
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(1);
    });

    it('03 - A list with 1 item should return a list with 0 item if it is not between the date from and date to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T00:00:00')
            }       
        ];
        
        var dateFrom = new Date('2015-10-12T10:15:12'); // Time will be ignored.
        var dateTo = new Date('2016-12-12T10:15:12'); // Time will be ignored.
        var timeFrom = null;
        var timeTo = null;
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(0);
    });
    
    it('04 - A list with 3 item should return a list with 1 item if only one of them is between the date from and date to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T00:00:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T00:00:00')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T00:00:00')
            }               
        ];
        
        var dateFrom = new Date('2015-10-12T10:15:12'); // Time will be ignored.
        var dateTo = new Date('2016-12-12T10:15:12'); // Time will be ignored.
        var timeFrom = null;
        var timeTo = null;
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(1);
    });
    
    it('05 - A list with 3 item should return a list with 2 item if the first one is previous to date from', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T00:00:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T00:00:00')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T00:00:00')
            }               
        ];
        
        var dateFrom = new Date('2015-10-12T10:15:12'); // Time will be ignored.
        var dateTo = null;
        var timeFrom = null;
        var timeTo = null;
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(2);
    });

    it('06 - A list with 3 item should return a list with 2 item if the last one is post to date to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T00:00:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T00:00:00')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T00:00:00')
            }               
        ];
        
        var dateFrom = null; 
        var dateTo = new Date('2017-01-15T10:15:12'); // Time will be ignored.
        var timeFrom = null;
        var timeTo = null;
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(2);
    });
    
    //---------------------------------------------------------------------------
    // Time cases.

    it('07 - A list with 1 item should return a list with 1 item if it is between the time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2016-12-10T10:15:12')
            }       
        ];
        
        var dateFrom = null; 
        var dateTo = null;
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(1);
    });

    it('08 - A list with 1 item should return a list with 0 item if it is not between the time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2016-12-10T10:15:12')
            }       
        ];
        
        var dateFrom = null; 
        var dateTo = null;
        var timeFrom = new Date('2015-10-12T11:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T11:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(0);
    });
    
    it('09 - A list with 3 item should return a list with 1 item if only one of them is between the time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T10:17:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T10:35:10')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T10:52:36')
            }               
        ];
        
        var dateFrom = null; 
        var dateTo = null;
        var timeFrom = new Date('2015-10-12T10:18:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:38:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(1);
    });
    
    it('10 - A list with 3 item should return a list with 2 item if the first one is previous to time from', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T10:17:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T10:35:10')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T10:52:36')
            }               
        ];
        
        var dateFrom = null; 
        var dateTo = null;
        var timeFrom = new Date('2015-10-12T10:18:12'); // Date will be ignored.
        var timeTo = null;
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(2);
    });

    it('11 - A list with 3 item should return a list with 2 item if the last one is post to time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-12-10T10:17:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T10:35:10')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T10:52:36')
            }               
        ];
        
        var dateFrom = null; 
        var dateTo = null;
        var timeFrom = null
        var timeTo = new Date('2016-12-12T10:38:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(2);
    });
    
    //---------------------------------------------------------------------------
    // Date and Time cases.
    
    it('12 - A list with 1 item should return a list with 1 item if it is between the date from and date to and between time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2016-12-10T10:15:12')
            }       
        ];
        
        var dateFrom = new Date('2015-10-12T10:10:12'); // Time will be ignored.
        var dateTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(1);
    });

    it('13 - A list with 1 item should return a list with 0 item if it is not between the date from and date to but between time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2016-12-15T10:15:12')
            }       
        ];
        
        var dateFrom = new Date('2015-10-12T10:10:12'); // Time will be ignored.
        var dateTo = new Date('2016-12-12T10:18:12'); // Time will be ignored.
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(0);
    });
    
    it('14 - A list with 1 item should return a list with 0 item if it is between the date from and date to but not between time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2016-11-10T15:15:12')
            }       
        ];
        
        var dateFrom = new Date('2015-10-12T10:10:12'); // Time will be ignored.
        var dateTo = new Date('2016-12-12T10:18:12'); // Time will be ignored.
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(0);
    });
    
    it('15 - A list with 3 item should return a list with 1 item if only one of them is between the date from and date to and between time from and time to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-07-10T10:17:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T10:35:10')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T10:52:36')
            }               
        ];
        
        var dateFrom = new Date('2015-10-12T10:10:12'); // Time will be ignored.
        var dateTo = new Date('2017-12-12T10:18:12'); // Time will be ignored.
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(1);
    });
    
    it('16 - A list with 3 item should return a list with 2 item if only two of them is between time from and time to and they are post date from', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-07-10T10:17:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T10:15:10')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T10:52:36')
            }               
        ];
        
        var dateFrom = new Date('2015-10-12T10:10:12'); // Time will be ignored.
        var dateTo = null;
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:18:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(2);
    });
    
    it('17 - A list with 3 item should return a list with 2 item if only two of them is between time from and time to and they are previous date to', function() {
        var items = [ 
            {
                _id: 1,
                datetime: new Date('2017-07-10T10:17:00')
            },
            {
                _id: 2,
                datetime: new Date('2016-12-10T10:15:10')
            },
            {
                _id: 3,
                datetime: new Date('2014-12-10T10:52:36')
            }               
        ];
        
        var dateFrom = null;
        var dateTo = new Date('2016-12-20T10:10:12'); // Time will be ignored.
        var timeFrom = new Date('2015-10-12T10:10:12'); // Date will be ignored.
        var timeTo = new Date('2016-12-12T10:55:12'); // Date will be ignored.
        
        var res = datetimefilter(items, dateFrom, dateTo, timeFrom, timeTo);
        expect(res.length).toBe(2);
    });

});
