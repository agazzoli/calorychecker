exports.config = {
    allScriptsTimeout: 11000,

    seleniumAddress: 'http://localhost:4444/wd/hub',

    specs: [
        'e2e/login_spec.js',
        'e2e/signup_spec.js',
        'e2e/users/add-user-dialog_spec.js',
        'e2e/users/modify-user-dialog_spec.js',
        'e2e/users/change-user-password-dialog_spec.js',
        'e2e/users/users_spec.js',
        'e2e/toolbar/logout_spec.js',
        'e2e/toolbar/modify-user-dialog_spec.js',
        'e2e/toolbar/change-user-password-dialog_spec.js',
        'e2e/meals/add-meal-dialog_spec.js',
        'e2e/meals/modify-meal-dialog_spec.js',
        'e2e/meals/meals_spec.js',
        'e2e/meals/meals_filtering_spec.js'
    ],

    capabilities: {
        'browserName': 'chrome'
    },

    chromeOnly: true,

    framework: 'jasmine',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000,
        print: function() {}
    },
    onPrepare: function() {
        var SpecReporter = require('jasmine-spec-reporter');
        // add jasmine spec reporter
        jasmine.getEnv().addReporter(new SpecReporter({}));
    }
};
