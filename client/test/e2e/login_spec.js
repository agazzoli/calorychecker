var checks = require('./helpers/checks.js');
var utils = require('./helpers/utils.js');

describe('Log in', function() {

    beforeEach(function() {
        browser.get('https://localhost:8080');
    });

    // Required fields.
    it('01 - Should all the fields be mandatory', function() {
        checks.mandatory('username');
        checks.mandatory('password');
    });

    // Default field values.
    it('02 - Should all the fields be empties by default', function() {
        checks.empty('username');
        checks.empty('password');
    });

    // Default error msg states.
    it('03 - Should there be no error by default', function() {
        checks.displayed('username_error_msg', false);
        checks.displayed('password_error_msg', false);
    });

    // Username validations.
    it('04 - Should show an error if username is empty after starting to write', function() {
        checks.required_error_msg_is_shown('username', 'username_error_msg', 'username_required_error_msg', 'Username is required.');
    });

    // Password validations.
    it('05 - Should show an error if password is empty after starting to write', function() {
        checks.required_error_msg_is_shown('password', 'password_error_msg', 'password_required_error_msg', 'Password is required.');
    });

    it('06 - Should log in with default admin user and password and go to the users page', function() {
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('admin');

        element(by.id('login_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');
    });
    
    it('07 - Should log in with a new user and go to the meals page', function() {
        // Create new user.
        var username = utils.get_random_username();

        element(by.id('create_new_account_link')).click();

        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('mypass');
        element(by.model('password2')).sendKeys('mypass');
        element(by.model('calories_expected_per_day')).sendKeys('2000');
        
        element(by.id('signup_btn')).click();

        var msg = element(by.id('new_account_created_msg'));
        expect(msg.isDisplayed()).toBe(true);
        
        // Login with new user.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('mypass');

        element(by.id('login_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/meals/' + username);
    });

    it('08 - Should not log in admin user with wrong password', function() {
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('wrong password');

        element(by.id('login_btn')).click();
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/');
        
        expect(element(by.id('login_error_msg')).getText()).toEqual('Error. Username or password wrong!')
    });
    
    it('09 - Should not log in invalid user', function() {
        element(by.model('username')).sendKeys('andres');
        element(by.model('password')).sendKeys('wrong password');

        element(by.id('login_btn')).click();
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/');

        expect(element(by.id('login_error_msg')).getText()).toEqual('Error. Username or password wrong!')
    });
    
    it('10 - Should navigate to sign up page from login page', function() {
        element(by.id('create_new_account_link')).click();
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/signup');
    });
});