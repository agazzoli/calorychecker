module.exports = {
    get_random_username: function() {
        var username = "";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            username += chars.charAt(Math.floor(Math.random() * chars.length));

        return username;
    },
    login_with_admin_user: function() {
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('admin');
        
        element(by.id('login_btn')).click();
        
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');
    },
    create_new_user: function() {
        // Login with admin user.
        this.login_with_admin_user();
        
        var username = this.get_random_username();        
        
        element(by.model('query.username')).sendKeys(username);
        expect(element.all(by.repeater('user in users')).count()).toBe(0);
        element(by.model('query.username')).clear();
        
        element(by.id('create_new_user_btn')).click();
        expect(element(by.id('add_user_dialog')).isPresent()).toBe(true);

        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('pass');
        element(by.model('password2')).sendKeys('pass');
        element(by.model('calories_expected_per_day')).clear();
        element(by.model('calories_expected_per_day')).sendKeys('2000');

        element(by.id('add_user_btn')).click();
        
        return username;
    },
    login: function(username) {
        // Log in with the new user.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('pass');
        element(by.id('login_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/meals/' + username);
    },
    add_new_meal: function(date, description, calories) {
        element(by.id('add_new_meal_btn')).click();

        element(by.id('meal_date')).clear();
        element(by.id('meal_date')).sendKeys(date);
        element(by.model('description')).sendKeys(description);
        element(by.model('calories')).clear();
        element(by.model('calories')).sendKeys(calories);

        element(by.id('add_modify_meal_btn')).click();
    }
};
