module.exports = {
    mandatory: function(field_model) {
        expect(element(by.model(field_model)).getAttribute('required')).toEqual('true');
    },
    mandatory_by_id: function(elem_id) {
        expect(element(by.id(elem_id)).getAttribute('required')).toEqual('true');
    },
    value: function(field_model, value) {
        expect(element(by.model(field_model)).getAttribute('value')).toEqual(value);
    },
    value_by_id: function(elem_id, value) {
        expect(element(by.id(elem_id)).getAttribute('value')).toEqual(value);
    },
    empty: function(field_model) {
        this.value(field_model, '');
    },
    displayed: function(elem_id, value) {
        expect(element(by.id(elem_id)).isDisplayed()).toEqual(value);
    },
    present: function(elem_id, value) {
        expect(element(by.id(elem_id)).isPresent()).toBe(value);
    },
    text: function(elem_id, value) {
        expect(element(by.id(elem_id)).getText()).toEqual(value);
    },
    error_msg_is_shown: function(error_msg_section_id, error_msg_container_id, error_msg) {
        this.displayed(error_msg_section_id, true);
        this.present(error_msg_container_id, true);
        
        this.text(error_msg_container_id, error_msg);
    },
    required_error_msg_is_shown: function(field_model, error_msg_section_id, error_msg_container_id, error_msg) {
        element(by.model(field_model)).sendKeys('123');
        element(by.model(field_model)).clear();
        this.error_msg_is_shown(error_msg_section_id, error_msg_container_id, error_msg);
    },
    required_error_msg_is_shown_by_id: function(elem_id, error_msg_section_id, error_msg_container_id, error_msg) {
        element(by.id(elem_id)).sendKeys('123');
        element(by.id(elem_id)).clear();
        this.error_msg_is_shown(error_msg_section_id, error_msg_container_id, error_msg);
    },
    value_error_msg_is_shown: function(field_model, error_msg_section_id, error_msg_container_id, error_msg, data) {
        element(by.model(field_model)).clear();
        element(by.model(field_model)).sendKeys(data);
        this.error_msg_is_shown(error_msg_section_id, error_msg_container_id, error_msg);
    },
    password_matching_error_msg_is_shown: function(password_model, password2_model, error_msg_section_id, error_msg_container_id, error_msg) {
        element(by.model(password_model)).sendKeys('pass');
        element(by.model(password2_model)).sendKeys('xxxx');
        this.error_msg_is_shown(error_msg_section_id, error_msg_container_id, error_msg);
    }
};
