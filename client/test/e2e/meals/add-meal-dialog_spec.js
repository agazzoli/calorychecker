var checks = require('../helpers/checks.js');
var utils = require('../helpers/utils.js');
var moment = require('../../../app/bower_components/moment/moment.js');

describe('Meals - Add Meal Dialog', function() {

    beforeEach(function() {
        var username = utils.create_new_user();
        
        browser.get('https://localhost:8080');

        // Log in with admin user and the new password.
        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('pass');

        element(by.id('login_btn')).click();

        element(by.id('add_new_meal_btn')).click();
        
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/meals/' + username);
    });

    //------------------------------------------------------------
    // Basic checks.
    
    // Required fields.
    it('01 - Should all the fields be mandatory', function() {
        checks.mandatory_by_id('meal_date');
        checks.mandatory_by_id('meal_time');
        checks.mandatory('description');
        checks.mandatory('calories');
    });
    
    // Default field values.
    it('02 - Should all the fields be empties or have the right default value', function() {
        // TODO. Time also should be checked here.
        checks.value_by_id('meal_date', moment(new Date()).format('MM/DD/YYYY'));
        checks.empty('description');
        checks.value('calories', '0');
    });
    
    // Default error msg states.
    it('03 - Should there be no error by default', function() {
        checks.displayed('datetime_error_msg', false);
        checks.displayed('description_error_msg', false);
        checks.displayed('calories_error_msg', false);
    });
    
    //------------------------------------------------------------
    // Form validations.
    
    // Datetime validations.
    it('04 - Should show an error if datetime is not valid', function() {
        checks.required_error_msg_is_shown_by_id('meal_date', 'datetime_error_msg', 'datetime_invalid_error_msg', 'Datetime is invalid.');
    });

    // Description validations.
    it('05 - Should show an error if description is empty after starting to write', function() {
        checks.required_error_msg_is_shown('description', 'description_error_msg', 'description_required_error_msg', 'Description is required.');
    });
    
    it('06 - Should show an error if description is longer than 200 characters', function() {
        var description = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
        checks.value_error_msg_is_shown('description', 'description_error_msg', 'description_too_long_error_msg', 'Description is too long.', description);
    });
    
    // Calories validations.
    it('07 - Should show an error if calories is empty after starting to write', function() {
        checks.required_error_msg_is_shown('calories', 'calories_error_msg', 'calories_required_error_msg', 'Calories is required.');
    });
    
    it('08 - Should show an error if calories is less than 0', function() {
        checks.value_error_msg_is_shown('calories', 'calories_error_msg', 'calories_min_error_msg', 'Calories should be equal or greater than 0.', '-1');
    });

    it('09 - Should show an error if calories is greater than 10000', function() {
        checks.value_error_msg_is_shown('calories', 'calories_error_msg', 'calories_max_error_msg', 'Calories should be equal or less than 10000.', '10001');
    });
});