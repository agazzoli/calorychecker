var checks = require('../helpers/checks.js');
var utils = require('../helpers/utils.js');

describe('Meals - Modify Meal Dialog', function() {

    var formatted_date = '';
    var description = '';
    var calories = 0;
    
    beforeEach(function() {
        var username = utils.create_new_user();
        
        browser.get('https://localhost:8080');

        // Log in with admin user and the new password.
        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('pass');

        element(by.id('login_btn')).click();
        
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/meals/' + username);

        // Add meal to modify.
        utils.add_new_meal('12/15/2016', 'salad', 500);

        formatted_date = element(by.repeater('meal in meals').row(0).column('meal.formatted_date')).getText();
        expect(formatted_date).toBe('12/15/2016');

        description = element(by.repeater('meal in meals').row(0).column('meal.description')).getText();
        expect(description).toBe('salad');

        calories = element(by.repeater('meal in meals').row(0).column('meal.calories')).getText();
        expect(calories).toBe('500');

        element(by.repeater('meal in meals').row(0)).all(by.css('._modify_meal_btn')).get(0).click();
    });

    //------------------------------------------------------------
    // Basic checks.
    
    // Required fields.
    it('01 - Should all the fields be mandatory', function() {
        checks.mandatory_by_id('meal_date');
        checks.mandatory_by_id('meal_time');
        checks.mandatory('description');
        checks.mandatory('calories');
    });
    
    // Default field values.
    it('02 - Should all the fields show the right values by default', function() {
        // TODO. Time also should be checked here.
        checks.value_by_id('meal_date', formatted_date);
        checks.value('description', 'salad');
        checks.value('calories', '500');
    });
    
    // Default error msg states.
    it('03 - Should there be no error by default', function() {
        checks.displayed('datetime_error_msg', false);
        checks.displayed('description_error_msg', false);
        checks.displayed('calories_error_msg', false);
    });
    
    //------------------------------------------------------------
    // Form validations.
    
    // Datetime validations.
    it('04 - Should show an error if datetime is not valid', function() {
        checks.required_error_msg_is_shown_by_id('meal_date', 'datetime_error_msg', 'datetime_invalid_error_msg', 'Datetime is invalid.');
    });

    // Description validations.
    it('05 - Should show an error if description is empty after starting to write', function() {
        checks.required_error_msg_is_shown('description', 'description_error_msg', 'description_required_error_msg', 'Description is required.');
    });
    
    it('06 - Should show an error if description is longer than 200 characters', function() {
        var description = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
        checks.value_error_msg_is_shown('description', 'description_error_msg', 'description_too_long_error_msg', 'Description is too long.', description);
    });
    
    // Calories validations.
    it('07 - Should show an error if calories is empty after starting to write', function() {
        checks.required_error_msg_is_shown('calories', 'calories_error_msg', 'calories_required_error_msg', 'Calories is required.');
    });
    
    it('08 - Should show an error if calories is less than 0', function() {
        checks.value_error_msg_is_shown('calories', 'calories_error_msg', 'calories_min_error_msg', 'Calories should be equal or greater than 0.', '-1');
    });

    it('09 - Should show an error if calories is greater than 10000', function() {
        checks.value_error_msg_is_shown('calories', 'calories_error_msg', 'calories_max_error_msg', 'Calories should be equal or less than 10000.', '10001');
    });
});