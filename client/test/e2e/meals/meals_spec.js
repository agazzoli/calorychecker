var checks = require('../helpers/checks.js');
var utils = require('../helpers/utils.js');
var moment = require('../../../app/bower_components/moment/moment.js');

describe('Meals', function() {

    // Add a meal.
    it('01 - Should add a meal', function() {
        var username = utils.create_new_user();
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);

        var description = element(by.repeater('meal in meals').row(0).column('meal.description')).getText();
        expect(description).toBe('salad');

        var calories = element(by.repeater('meal in meals').row(0).column('meal.calories')).getText();
        expect(calories).toBe('500');
    });
    
    // Delete a meal.
    it('02 - Should delete a meal', function() {
        var username = utils.create_new_user();
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);

        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        element(by.repeater('meal in meals').row(0)).all(by.css('._delete_meal_btn')).get(0).click();
        element(by.id('confirmation_ok_btn')).click();

        expect(element.all(by.repeater('meal in meals')).count()).toBe(0);
    });
    
    // Modify a meal.
    it('03 - Should modify a meal', function() {
        var username = utils.create_new_user();
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);

        var description = element(by.repeater('meal in meals').row(0).column('meal.description')).getText();
        expect(description).toBe('salad');

        var calories = element(by.repeater('meal in meals').row(0).column('meal.calories')).getText();
        expect(calories).toBe('500');
        
        element(by.repeater('meal in meals').row(0)).all(by.css('._modify_meal_btn')).get(0).click();
        
        element(by.model('description')).clear();
        element(by.model('description')).sendKeys('pasta');
        element(by.model('calories')).clear();
        element(by.model('calories')).sendKeys('2500');
        
        element(by.id('add_modify_meal_btn')).click();
        
        description = element(by.repeater('meal in meals').row(0).column('meal.description')).getText();
        expect(description).toBe('pasta');

        calories = element(by.repeater('meal in meals').row(0).column('meal.calories')).getText();
        expect(calories).toBe('2500');
    });
    
    it('04 - Should be shown a green bar when calories expected per day are less or equal than the calories in the current day', function() {
        var username = utils.create_new_user(); // Calories expected per day: 2000
        utils.login(username);
        utils.add_new_meal(moment(new Date()).format('MM/DD/YYYY'), 'salad', 500);
        
        expect(element(by.css('._calories_expected_row')).getAttribute('style')).toBe('background-color: rgb(113, 216, 210);');
    });
            
    it('05 - Should be shown a red bar when calories expected per day are more than the calories in the current day', function() {
        var username = utils.create_new_user(); // Calories expected per day: 2000
        utils.login(username);
        utils.add_new_meal(moment(new Date()).format('MM/DD/YYYY'), 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal(moment(new Date()).format('MM/DD/YYYY'), 'pasta', 2100);
        
        expect(element(by.css('._calories_expected_row')).getAttribute('style')).toBe('background-color: rgb(239, 120, 147);')
    });
  
});