var checks = require('../helpers/checks.js');
var utils = require('../helpers/utils.js');

describe('Meals - Filtering', function() {

    it('01 - Should not show any meal if the date from is set to 2020', function() {
        var username = utils.create_new_user(); 
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal('12/15/2016', 'pasta', 2100);
        
        expect(element.all(by.repeater('meal in meals')).count()).toBe(2);
        
        element(by.model('dateFrom')).sendKeys('01/01/2020');
        
         expect(element.all(by.repeater('meal in meals')).count()).toBe(0);
    });
    
    it('02 - Should show only one meal if the date from is set to 12/13/2016', function() {
        var username = utils.create_new_user(); 
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal('12/15/2016', 'pasta', 2100);
        
        expect(element.all(by.repeater('meal in meals')).count()).toBe(2);
        
        element(by.model('dateFrom')).sendKeys('12/13/2016');
        
         expect(element.all(by.repeater('meal in meals')).count()).toBe(1);
    });
    
    it('03 - Should not show any meal if the date to is set to 2010', function() {
        var username = utils.create_new_user(); 
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal('12/15/2016', 'pasta', 2100);
        
        expect(element.all(by.repeater('meal in meals')).count()).toBe(2);
        
        element(by.model('dateTo')).sendKeys('01/01/2010');
        
         expect(element.all(by.repeater('meal in meals')).count()).toBe(0);
    });
    
    it('04 - Should show only one meal if the date to is set to 12/13/2016', function() {
        var username = utils.create_new_user(); 
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal('12/15/2016', 'pasta', 2100);
        
        expect(element.all(by.repeater('meal in meals')).count()).toBe(2);
        
        element(by.model('dateTo')).sendKeys('12/13/2016');
        
         expect(element.all(by.repeater('meal in meals')).count()).toBe(1);
    });
    
    it('05 - Should show only one meal if the date from is set to 12/13/2016 and the date to is set to 12/15/2016', function() {
        var username = utils.create_new_user(); 
        utils.login(username);
        utils.add_new_meal('12/12/2016', 'salad', 500);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal('12/14/2016', 'pasta', 2100);
        
        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(500);
        
        utils.add_new_meal('12/20/2016', 'steak', 2500);
        
        expect(element.all(by.repeater('meal in meals')).count()).toBe(3);
        
        element(by.model('dateTo')).sendKeys('12/13/2016');
        
        expect(element.all(by.repeater('meal in meals')).count()).toBe(1);
    });

    // TODO. Time From and Time To also should be checked here.
});