var checks = require('../helpers/checks.js');

describe('Users - Add User Dialog', function() {

    beforeEach(function() {
        // Login with admin user.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('admin');
        
        element(by.id('login_btn')).click();
        
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');
        
        element(by.id('create_new_user_btn')).click();
    });

    //------------------------------------------------------------
    // Basic checks.
    
    // Required fields.
    it('01 - Should all the fields be mandatory', function() {
        checks.mandatory('username');
        checks.mandatory('password');
        checks.mandatory('password2');
        checks.mandatory('calories_expected_per_day');
    });
    
    // Default field values.
    it('02 - Should all the fields be empties or with 0 by default', function() {
        checks.empty('username');
        checks.empty('password');
        checks.empty('password2');
        checks.value('calories_expected_per_day', '0');
    });
    
    // Default error msg states.
    it('03 - Should there be no error by default', function() {
        checks.displayed('username_error_msg', false);
        checks.displayed('password_error_msg', false);
        checks.displayed('password2_error_msg', false);
        checks.displayed('calories_expected_per_day_error_msg', false);
    });
    
    //------------------------------------------------------------
    // Form validations.
    
    // Username validations.
    it('04 - Should show an error if username is empty after starting to write', function() {
        checks.required_error_msg_is_shown('username', 'username_error_msg', 'username_required_error_msg', 'Username is required.');
     });
    
    it('05 - Should show an error if username is longer than 15 characters', function() {
        checks.value_error_msg_is_shown('username', 'username_error_msg', 'username_too_long_error_msg', 'Username is too long.', '01234567890123456');
    });
    
    // Password validations.
    it('06 - Should show an error if password is empty after starting to write', function() {
        checks.required_error_msg_is_shown('password', 'password_error_msg', 'password_required_error_msg', 'Password is required.');
    });
     
    it('07 - Should show an error if password is shorter than 4 characters', function() {
        checks.value_error_msg_is_shown('password', 'password_error_msg', 'password_too_short_error_msg', 'Password is too short.', 'pw');
    });
    
    it('08 - Should show an error if password is longer than 10 characters', function() {
        checks.value_error_msg_is_shown('password', 'password_error_msg', 'password_too_long_error_msg', 'Password is too long.', '01234567890123');
    });
    
    // Password2 validations.
    it('09 - Should show an error if password2 is empty after starting to write', function() {
        checks.required_error_msg_is_shown('password2', 'password2_error_msg', 'password2_required_error_msg', 'Password is required.');
    });
    
    it('10 - Should show an error if password and password2 do not match', function() {
        checks.password_matching_error_msg_is_shown('password', 'password2', 'password2_error_msg', 'passwords_doesnt_match_error_msg', "Passwords doesn't match.");
    });
    
    // Calories expected per day validations.
    it('11 - Should show an error if calories_expected_per_day is empty after starting to write', function() {
        checks.required_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_required_error_msg', 'Calories expected per day is required.');
    });
    
    it('12 - Should show an error if calories_expected_per_day is less than 0', function() {
        checks.value_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_min_error_msg', 'Calories expected per day should be equal or greater than 0.', '-1');
    });

    it('13 - Should show an error if calories_expected_per_day is greater than 10000', function() {
        checks.value_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_max_error_msg', 'Calories expected per day should be equal or less than 10000.', '10001');
    });
});