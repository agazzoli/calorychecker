var checks = require('../helpers/checks.js');

describe('Users - Modify User Dialog', function() {

    var username = '';
    var calories_expected_per_day = 0;

    beforeEach(function() {
        // Login with admin user.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('admin');
        
        element(by.id('login_btn')).click();
        
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');

        username = element(by.repeater('user in users').row(0).column('user.username')).getText();
        calories_expected_per_day = element(by.repeater('user in users').row(0).column('user.calories_expected_per_day')).getText();
        element(by.repeater('user in users').row(0)).all(by.css('._modify_user_btn')).get(0).click();
    });

    //------------------------------------------------------------
    // Basic checks.
    
    // Required fields.
    it('01 - Should calories_expected_per_day be mandatory', function() {
        checks.mandatory('calories_expected_per_day');
    });
    
    // Default field values.
    it('02 - Should username have a the right username by default', function() {
        checks.value('username', username);
    });

    it('03 - Should calories_expected_per_day have the right calories number by default', function() {
        checks.value('calories_expected_per_day', calories_expected_per_day);
    });
    
    // Default error msg states.
    it('04 - Should there be no error by default', function() {
        checks.displayed('calories_expected_per_day_error_msg', false);
    });
    
    //------------------------------------------------------------
    // Form validations.

    // Calories expected per day validations.
    it('05 - Should show an error if calories_expected_per_day is empty after starting to write', function() {
        checks.required_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_required_error_msg', 'Calories expected per day is required.');
    });
    
    it('06 - Should show an error if calories_expected_per_day is less than 0', function() {
        checks.value_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_min_error_msg', 'Calories expected per day should be equal or greater than 0.', '-1');
    });

    it('07 - Should show an error if calories_expected_per_day is greater than 10000', function() {
        checks.value_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_max_error_msg', 'Calories expected per day should be equal or less than 10000.', '10001');
    });
});