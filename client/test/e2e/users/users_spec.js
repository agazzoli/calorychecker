var checks = require('../helpers/checks.js');
var utils = require('../helpers/utils.js');

describe('Users', function() {

    it('01 - Should create a new user', function() {
        var username = utils.create_new_user();

        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(1500);
     
        element(by.model('query.username')).sendKeys(username);
        expect(element.all(by.repeater('user in users')).count()).toBe(1);
        
        var username_in_table = element(by.repeater('user in users').row(0).column('user.username')).getText();
        expect(username_in_table).toBe(username);

        var calories_expected_per_day_in_table = element(by.repeater('user in users').row(0).column('user.calories_expected_per_day')).getText();
        expect(calories_expected_per_day_in_table).toBe('2000');
    });

    it('02 - Should delete an user', function() {
        var username = utils.create_new_user();

        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(1500);

        element(by.model('query.username')).sendKeys(username);
        expect(element.all(by.repeater('user in users')).count()).toBe(1);

        element(by.repeater('user in users').row(0)).all(by.css('._delete_user_btn')).get(0).click();
        element(by.id('confirmation_ok_btn')).click();

        expect(element.all(by.repeater('user in users')).count()).toBe(0);
    });

    it('03 - Should modify an user', function() {
        var username = utils.create_new_user();

        // This sleep is because sometimes when I run this test along with all the set of test cases
        // protractor doesn't find an element and fails.
        browser.sleep(1500);

        element(by.model('query.username')).sendKeys(username);
        expect(element.all(by.repeater('user in users')).count()).toBe(1);

        var calories_expected_per_day_in_table = element(by.repeater('user in users').row(0).column('user.calories_expected_per_day')).getText();
        expect(calories_expected_per_day_in_table).toBe('2000');

        element(by.repeater('user in users').row(0)).all(by.css('._modify_user_btn')).get(0).click();

        element(by.model('calories_expected_per_day')).clear();
        element(by.model('calories_expected_per_day')).sendKeys('3000');
        element(by.id('modify_user_btn')).click();

        var calories_expected_per_day_in_table = element(by.repeater('user in users').row(0).column('user.calories_expected_per_day')).getText();
        expect(calories_expected_per_day_in_table).toBe('3000');
    });

    it('04 - Should modify logged user', function() {
        // Log in with admin user.
        utils.login_with_admin_user();
        
        element(by.model('query.username')).sendKeys('admin');
        expect(element.all(by.repeater('user in users')).count()).toBe(1);

        // First modification to set a well-known value;
        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_settings')).click();

        element(by.model('calories_expected_per_day')).clear();
        element(by.model('calories_expected_per_day')).sendKeys('2000');
        element(by.id('modify_user_btn')).click();

        var calories_expected_per_day_in_table = element(by.repeater('user in users').row(0).column('user.calories_expected_per_day')).getText();
        expect(calories_expected_per_day_in_table).toBe('2000');

        // Second modification. To be sure that the value was really changed.
        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_settings')).click();

        element(by.model('calories_expected_per_day')).clear();
        element(by.model('calories_expected_per_day')).sendKeys('3000');
        element(by.id('modify_user_btn')).click();

        var calories_expected_per_day_in_table = element(by.repeater('user in users').row(0).column('user.calories_expected_per_day')).getText();
        expect(calories_expected_per_day_in_table).toBe('3000');
    });
    
    it('05 - Should change logged in admin user password', function() {
        // Log in with admin user.
        utils.login_with_admin_user();

        // Change password.
        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_password')).click();

        element(by.model('current_password')).sendKeys('admin');
        element(by.model('new_password')).sendKeys('newpass');
        element(by.model('new_password2')).sendKeys('newpass');

        element(by.id('change_user_password_btn')).click();
        
        // Confirmation message.
        expect(element(by.id('alert_msg')).isPresent()).toBe(true);
        expect(element(by.binding('data.text')).getText()).toBe("User's password was successfully changed");
        element(by.id('alert_close_btn')).click();
                        
        // Log in with admin user and the new password.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('newpass');

        element(by.id('login_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');

        // Restablish default password
        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_password')).click();

        element(by.model('current_password')).sendKeys('newpass');
        element(by.model('new_password')).sendKeys('admin');
        element(by.model('new_password2')).sendKeys('admin');

        element(by.id('change_user_password_btn')).click();
        
        // Confirmation message.
        expect(element(by.id('alert_msg')).isPresent()).toBe(true);
        expect(element(by.binding('data.text')).getText()).toBe("User's password was successfully changed");
        element(by.id('alert_close_btn')).click();
    });
    
    it('06 - Should change logged in non admin user password', function() {
        // Create a new user and log in with it.
        var username = utils.create_new_user();
        utils.login(username);
    
        // Change password.
        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_password')).click();
        
        element(by.model('current_password')).sendKeys('pass');
        element(by.model('new_password')).sendKeys('newpass');
        element(by.model('new_password2')).sendKeys('newpass');

        element(by.id('change_user_password_btn')).click();
        
        // Confirmation message.
        expect(element(by.id('alert_msg')).isPresent()).toBe(true);
        expect(element(by.binding('data.text')).getText()).toBe("User's password was successfully changed");
        element(by.id('alert_close_btn')).click();
        
        // Log in with the new user and the new password.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('newpass');

        element(by.id('login_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/meals/' + username);
    });

    it('07 - Should show an error when trying to change logged in non admin user password with a wrong current password', function() {
        // Create a new user and log in with it.
        var username = utils.create_new_user();
        utils.login(username);
        
        // Change password.
        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_password')).click();

        element(by.model('current_password')).sendKeys('wrongpass');
        element(by.model('new_password')).sendKeys('newpass');
        element(by.model('new_password2')).sendKeys('newpass');

        element(by.id('change_user_password_btn')).click();

        // Error message.
        expect(element(by.id('alert_msg')).isPresent()).toBe(true);
        expect(element(by.binding('data.text')).getText()).toBe('Wrong password!');
        element(by.id('alert_close_btn')).click();
    });
});