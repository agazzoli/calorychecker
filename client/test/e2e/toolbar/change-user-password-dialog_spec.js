var checks = require('../helpers/checks.js');
var utils = require('../helpers/utils.js');

describe('Toolbar - Change User Password Dialog', function() {

    describe('Admin user', function() {
        var username = 'admin';

        beforeEach(function() {
            // Login with admin user.
            browser.get('https://localhost:8080');
            element(by.model('username')).sendKeys('admin');
            element(by.model('password')).sendKeys('admin');

            element(by.id('login_btn')).click();

            expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');

            element(by.css('.md-icon-button')).click();
            element(by.id('toolbar_change_user_password')).click();
        });

        //------------------------------------------------------------
        // Basic checks.

        // Required fields.
        it('01 - Should all the fields be mandatory', function() {
            checks.mandatory('new_password');
            checks.mandatory('new_password2');
        });

        // Default field values.
        it('02 - Should username have a the right username by default', function() {
            checks.value('username', 'admin');
        });

        it('03 - Should all the password fields be empties by default', function() {
            checks.empty('new_password');
            checks.empty('new_password2');
        });

        // Default error msg states.
        it('04 - Should there be no error by default', function() {
            checks.displayed('new_password_error_msg', false);
            checks.displayed('new_password2_error_msg', false);
        });

        //------------------------------------------------------------
        // Form validations.

        // New Password validations.
        it('05 - Should show an error if new password is empty after starting to write', function() {
            checks.required_error_msg_is_shown('new_password', 'new_password_error_msg', 'new_password_required_error_msg', 'Password is required.');
        });

        it('06 - Should show an error if new password is shorter than 4 characters', function() {
            checks.value_error_msg_is_shown('new_password', 'new_password_error_msg', 'new_password_too_short_error_msg', 'Password is too short.', 'pw');
        });

        it('07 - Should show an error if new password is longer than 10 characters', function() {
            checks.value_error_msg_is_shown('new_password', 'new_password_error_msg', 'new_password_too_long_error_msg', 'Password is too long.', '01234567890123');
        });

        // New Password2 validations.
        it('08 - Should show an error if new password2 is empty after starting to write', function() {
            checks.required_error_msg_is_shown('new_password2', 'new_password2_error_msg', 'new_password2_required_error_msg', 'Password is required.');
        });

        it('09 - Should show an error if new password and new password2 do not match', function() {
            checks.password_matching_error_msg_is_shown('new_password', 'new_password2', 'new_password2_error_msg', 'new_passwords_doesnt_match_error_msg', "Passwords doesn't match.");
        });
    });

    describe('Non Admin user', function() {

        // Create new user.
        browser.get('https://localhost:8080');
        var username = utils.get_random_username();

        element(by.id('create_new_account_link')).click();

        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys('mypass');
        element(by.model('password2')).sendKeys('mypass');
        element(by.model('calories_expected_per_day')).sendKeys('2000');

        element(by.id('signup_btn')).click();

        beforeEach(function() {
            // Login with new user.
            browser.get('https://localhost:8080');
            element(by.model('username')).sendKeys(username);
            element(by.model('password')).sendKeys('mypass');

            element(by.id('login_btn')).click();

            expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/meals/' + username);

            element(by.css('.md-icon-button')).click();
            element(by.id('toolbar_change_user_password')).click();
        });

        //------------------------------------------------------------
        // Basic checks.

        // Required fields.
        it('01 - Should all the fields be mandatory', function() {
            checks.mandatory('current_password');
            checks.mandatory('new_password');
            checks.mandatory('new_password2');
        });

        // Default field values.
        it('02 - Should username have a the right username by default', function() {
            checks.value('username', username);
        });

        it('03 - Should all the password fields be empties by default', function() {
            checks.empty('current_password');
            checks.empty('new_password');
            checks.empty('new_password2');
        });

        // Default error msg states.
        it('04 - Should there be no error by default', function() {
            checks.displayed('current_password_error_msg', false);
            checks.displayed('new_password_error_msg', false);
            checks.displayed('new_password2_error_msg', false);
        });

        //------------------------------------------------------------
        // Form validations.

        // Current Password validations.
        it('05 - Should show an error if current password is empty after starting to write', function() {
            checks.required_error_msg_is_shown('current_password', 'current_password_error_msg', 'current_password_required_error_msg', 'Password is required.');
        });

        // New Password validations.
        it('06 - Should show an error if new password is empty after starting to write', function() {
            checks.required_error_msg_is_shown('new_password', 'new_password_error_msg', 'new_password_required_error_msg', 'Password is required.');
        });

        it('07 - Should show an error if new password is shorter than 4 characters', function() {
            checks.value_error_msg_is_shown('new_password', 'new_password_error_msg', 'new_password_too_short_error_msg', 'Password is too short.', 'pw');
        });

        it('08 - Should show an error if new password is longer than 10 characters', function() {
            checks.value_error_msg_is_shown('new_password', 'new_password_error_msg', 'new_password_too_long_error_msg', 'Password is too long.', '01234567890123');
        });

        // New Password2 validations.
        it('09 - Should show an error if new password2 is empty after starting to write', function() {
            checks.required_error_msg_is_shown('new_password2', 'new_password2_error_msg', 'new_password2_required_error_msg', 'Password is required.');
        });

        it('10 - Should show an error if new password and new password2 do not match', function() {
            checks.password_matching_error_msg_is_shown('new_password', 'new_password2', 'new_password2_error_msg', 'new_passwords_doesnt_match_error_msg', "Passwords doesn't match.");
        });
    });
});