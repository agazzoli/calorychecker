var checks = require('../helpers/checks.js');

describe('Toolbar - Logout', function() {
    
    it('01 - Should the user be logged out', function() {
        // Login with admin user.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('admin');

        element(by.id('login_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');

        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_logout_btn')).click();

        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/');

        browser.manage().getCookie("auth_token").then(function(data) {
            expect(data).toBe(null);
        });

        browser.manage().getCookie("username").then(function(data) {
            expect(data).toBe(null);
        });

        browser.manage().getCookie("admin").then(function(data) {
            expect(data).toBe(null);
        });

        browser.manage().getCookie("calories_expected_per_day").then(function(data) {
            expect(data).toBe(null);
        });
    });
});