var checks = require('../helpers/checks.js');

describe('Toolbar - Modify User Dialog', function() {

    beforeEach(function() {
        // Login with admin user.
        browser.get('https://localhost:8080');
        element(by.model('username')).sendKeys('admin');
        element(by.model('password')).sendKeys('admin');
        
        element(by.id('login_btn')).click();
        
        expect(browser.getCurrentUrl()).toEqual('https://localhost:8080/#/users');

        element(by.css('.md-icon-button')).click();
        element(by.id('toolbar_change_user_settings')).click();

    });

    //------------------------------------------------------------
    // Basic checks.
    
    // Required fields.
    it('01 - Should calories_expected_per_day be mandatory', function() {
        checks.mandatory('calories_expected_per_day');
    });
    
    // Default field values.
    it('02 - Should username have a the right username by default', function() {
        checks.value('username', 'admin');
    });

    it('03 - Should calories_expected_per_day have the right calories number by default', function() {
        browser.manage().getCookie("calories_expected_per_day").then(function(data){
            checks.value('calories_expected_per_day', data.value);
        });
    });
    
    // Default error msg states.
    it('04 - Should there be no error by default', function() {
        checks.displayed('calories_expected_per_day_error_msg', false);
    });
    
    //------------------------------------------------------------
    // Form validations.

    // Calories expected per day validations.
    it('05 - Should show an error if calories_expected_per_day is empty after starting to write', function() {
        checks.required_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_required_error_msg', 'Calories expected per day is required.');
    });
    
    it('06 - Should show an error if calories_expected_per_day is less than 0', function() {
        checks.value_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_min_error_msg', 'Calories expected per day should be equal or greater than 0.', '-1');
    });

    it('07 - Should show an error if calories_expected_per_day is greater than 10000', function() {
        checks.value_error_msg_is_shown('calories_expected_per_day', 'calories_expected_per_day_error_msg', 'calories_expected_per_day_max_error_msg', 'Calories expected per day should be equal or less than 10000.', '10001');
    });
});