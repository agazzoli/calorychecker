module.exports = function(config){
    config.set({

        basePath : '../',

        files : [
            'app/bower_components/angular/angular.js',
            'app/bower_components/angular-messages/angular-messages.js',
            'app/bower_components/angular-route/angular-route.js',
            'app/bower_components/angular-cookies/angular-cookies.js',
            'app/bower_components/angular-animate/angular-animate.js',
            'app/bower_components/angular-resource/angular-resource.js',
            'app/bower_components/angular-password/angular-password.js',
            'app/bower_components/angular-aria/angular-aria.js',
            'app/bower_components/angular-material/angular-material.js',
            'app/bower_components/moment/moment.js',
            'app/lib/ui-bootstrap-tpls-2.1.3.min.js',

            'app/bower_components/angular-mocks/angular-mocks.js',

            'app/js/controllers/config.js',
            'app/js/services/config.js',
            
            'app/js/services/*.js',
            'app/js/filters/*.js',
            'app/js/controllers/*.js',
            'app/js/app.js',
                        
            'test/unit/**/*.js'
        ],

        autoWatch : true,

        frameworks: ['jasmine'],

        browsers : ['Chrome'],

        reporters : ['mocha'],

        plugins: [
            'karma-jasmine',
            'karma-mocha-reporter',
            'karma-chrome-launcher'
        ]
      });
};